package kh.com.soho.soho.Model;

public class LoginModel {
    private String status = "";
    private LoginDataModel data;

    public LoginModel(LoginDataModel data) {
        this.data = data;
    }

    public String getStatus(){
        return status;
    }

    public LoginDataModel getLoginData() {
        return data;
    }
}