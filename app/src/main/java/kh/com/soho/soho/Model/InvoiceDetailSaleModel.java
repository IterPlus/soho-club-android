package kh.com.soho.soho.Model;

import java.util.ArrayList;

public class InvoiceDetailSaleModel {
    private ArrayList<InvoiceItemModel> items;
    private ArrayList<InvoiceCollectionModel> collections;

    public ArrayList<InvoiceItemModel> getInvoiceItems(){
        return items;
    }

    public ArrayList<InvoiceCollectionModel> getCollections(){
        return collections;
    }
}
