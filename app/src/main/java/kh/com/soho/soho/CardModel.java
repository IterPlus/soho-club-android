package kh.com.soho.soho;

import android.os.Parcel;
import android.os.Parcelable;

class CardModel implements Parcelable{
    private String id;
    private String name;
    private String code;
    private String point;
    private String phone;
    private String dob;
    private String token;
    private String dkey;
    private String outlet;
    private int logo;
    private int imageView;

    public CardModel(String id, String name, String code, String point, String phone, String dob, String token, String dkey, String outlet, int logo, int imageView) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.point = point;
        this.phone = phone;
        this.dob = dob;
        this.token = token;
        this.dkey = dkey;
        this.outlet = outlet;
        this.logo = logo;
        this.imageView = imageView;
    }

    protected CardModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        code = in.readString();
        point = in.readString();
        phone = in.readString();
        dob = in.readString();
        token = in.readString();
        dkey = in.readString();
        outlet = in.readString();
        logo = in.readInt();
        imageView = in.readInt();
    }

    public static final Creator<CardModel> CREATOR = new Creator<CardModel>() {
        @Override
        public CardModel createFromParcel(Parcel in) {
            return new CardModel(in);
        }

        @Override
        public CardModel[] newArray(int size) {
            return new CardModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getPoint() {
        return point;
    }

    public String getPhone() {
        return phone;
    }

    public String getDob() {
        return dob;
    }

    public String getToken() {
        return token;
    }

    public String getDkey() {
        return dkey;
    }

    public String getOutlet() {
        return outlet;
    }

    public int getLogo() {
        return logo;
    }

    public int getImageView() {
        return imageView;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(code);
        dest.writeString(point);
        dest.writeString(phone);
        dest.writeString(dob);
        dest.writeString(token);
        dest.writeString(dkey);
        dest.writeString(outlet);
        dest.writeInt(logo);
        dest.writeInt(imageView);
    }
}
