package kh.com.soho.soho;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.r0adkll.slidr.Slidr;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import kh.com.soho.soho.InterfaceService.LoginService;
import kh.com.soho.soho.InterfaceService.RegisterService;
import kh.com.soho.soho.Model.LoginDataClientsModel;
import kh.com.soho.soho.Model.LoginModel;
import kh.com.soho.soho.Model.RegisterGetNameModel;
import kh.com.soho.soho.Model.RegisterUserModel;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static kh.com.soho.soho.Ulti.showMessage;

public class RegisterActivity extends AppCompatActivity {

    Toolbar mToolbar;
    EditText masterCardEditText, phoneEditText;
    Button verifyBtn;
    ProgressBar progressBar;
    String first_name, last_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mToolbar = findViewById(R.id.act_register_soho_toolbar);
        masterCardEditText = findViewById(R.id.act_register_mastercard_edittext);
        phoneEditText = findViewById(R.id.act_register_phone_edittext);
        verifyBtn = findViewById(R.id.act_register_verify_btn);
        progressBar = findViewById(R.id.act_register_progress_bar);
        Ulti.setupUI(findViewById(R.id.act_register_parent), RegisterActivity.this);
        setmToolbar();
//        masterCardEditText.setText("5363657327850639");
//        phoneEditText.setText("012345678");

//        masterCardEditText.setText("5363655004733573");
//        phoneEditText.setText("092916640");

        Slidr.attach(this);
        verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setProgressBarLoading();
                getAccessToken();
            }
        });
    }

    public void getAccessToken(){
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(15, TimeUnit.SECONDS);
        client.readTimeout(15, TimeUnit.SECONDS);
        client.writeTimeout(15, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Ulti.BaseUrlWing)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();

        RegisterService service = retrofit.create(RegisterService.class);
        Call call = service.getAccessToken();
        call.enqueue(new Callback<RegisterUserModel>() {
            @Override
            public void onResponse(@NonNull Call<RegisterUserModel> call, @NonNull retrofit2.Response<RegisterUserModel> response) {

                if (response.body() != null) {
                    Log.d("response.code().", "" + response.body().getAccessToken());
                    getUserName(response.body().getAccessToken(), masterCardEditText.getText().toString(), phoneEditText.getText().toString());
                }
            }

            @Override
            public void onFailure(Call<RegisterUserModel> call, Throwable t) {
                showMessage("No Internet Connection" + t, RegisterActivity.this);
            }
        });
    }

    public void getUserName(String token, String cardNo, String phone){
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(15, TimeUnit.SECONDS);
        client.readTimeout(15, TimeUnit.SECONDS);
        client.writeTimeout(15, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Ulti.BaseUrlWing)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();

        RegisterService service = retrofit.create(RegisterService.class);
        Call call = service.getKYCNumber(token, cardNo, phone);
        call.enqueue(new Callback<RegisterGetNameModel>() {
            @Override
            public void onResponse(@NonNull Call<RegisterGetNameModel> call, @NonNull retrofit2.Response<RegisterGetNameModel> response) {

                if (response.body() != null) {
                    Log.d("RegisterGetNameModel", "" + response.body().getFirstName());
                    if(response.body().getFirstName() != null && !response.body().getLastName().equals("")){
                        setProgressBarStop();
                        Intent intent = new Intent(RegisterActivity.this, CreateUserActivity.class);
                        intent.putExtra("first_name", response.body().getFirstName());
                        intent.putExtra("last_name", response.body().getLastName());
                        intent.putExtra("phone", phoneEditText.getText().toString());
                        startActivity(intent);
                    }else{
                        setProgressBarStop();
                        showMessage("Card isnot register via Wing.", RegisterActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterGetNameModel> call, Throwable t) {
                showMessage("No Internet Connection" + t, RegisterActivity.this);
            }
        });
    }

    public void setProgressBarLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void setProgressBarStop() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setmToolbar() {
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}
