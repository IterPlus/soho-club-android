package kh.com.soho.soho;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class HistoryDetailAdapter extends RecyclerView.Adapter<HistoryDetailAdapter.ViewHolder>{

    private List<HistoryDetailModel> historyDetailList;
    private Context context;
    String grandTotal = "";
    double grandDiscount = 0;
    public static final int FOOTER = 1;
    public static final int NORMAL = 0;

    public HistoryDetailAdapter(List<HistoryDetailModel> historyDetailList, Context context, String grandTotal) {
        this.historyDetailList = historyDetailList;
        this.context = context;
        this.grandTotal = grandTotal;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (viewType == FOOTER) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_history_detail, parent, false);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_history_detail, parent, false);
        }
        return new HistoryDetailAdapter.ViewHolder(v);
    }


    @Override
    public int getItemViewType(int position) {
        if (position == historyDetailList.size()) {
            return FOOTER;
        } else
            return NORMAL;

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position < historyDetailList.size()) {
            final HistoryDetailModel list = historyDetailList.get(position);
            holder.descriptionTxt.setText(list.getItemName());
            String a = ".0";
            String[] qty = list.getQuantity().split(a);
            holder.qtyTxt.setText(qty[0]);
            holder.priceTxt.setText("$" + list.getUnitPrice());
            holder.discountTxt.setText("$" + list.getDiscountAmount());
            holder.total.setText("$" + list.subTotal);
            grandDiscount = grandDiscount + Double.parseDouble(list.discountAmount);
        }else{
            holder.grandDiscountTxt.setText("$" + grandDiscount);
            holder.grandTotalTxt.setText("$" + grandTotal);
        }
    }

    @Override
    public int getItemCount() {
        return historyDetailList.size() + 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView descriptionTxt, qtyTxt, priceTxt, discountTxt, total, grandDiscountTxt, grandTotalTxt;
        ImageView outletLogoImg;
        ViewHolder(View itemView) {
            super(itemView);

            descriptionTxt = itemView.findViewById(R.id.act_history_detail_description_txt);
            qtyTxt = itemView.findViewById(R.id.act_history_detail_qty_txt);
            priceTxt = itemView.findViewById(R.id.act_history_detail_price_txt);
            discountTxt = itemView.findViewById(R.id.act_history_detail_discount_txt);
            total = itemView.findViewById(R.id.act_history_detail_total_txt);
            outletLogoImg = itemView.findViewById(R.id.act_history_detail_outlet_image);
            grandDiscountTxt = itemView.findViewById(R.id.act_history_detail_footer_discount_txt);
            grandTotalTxt = itemView.findViewById(R.id.act_history_detail_footer_grand_total_txt);
        }
    }
}