package kh.com.soho.soho.Model;

public class InvoiceItemModel {
    private double subtotal = 0.0;
    private String itemCode = "";
    private String itemName = "";
    private int quantity = 0;
    private int discountPercentage = 0;
    private String itemType = "";
    private double discountAmount = 0.0;
    private double unitPrice = 0.0;

    public double getSubtotal(){
        return subtotal;
    }

    public double getDiscountAmount(){
        return discountAmount;
    }

    public double getUnitPrice(){
        return unitPrice;
    }

    public int getQuantity(){
        return quantity;
    }

    public int getDiscountPercentage(){
        return discountPercentage;
    }

    public String getItemCode(){
        return itemCode;
    }

    public String getItemName(){
        return itemName;
    }

    public String getItemType(){
        return itemType;
    }
}