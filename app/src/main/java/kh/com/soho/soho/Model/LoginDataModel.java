package kh.com.soho.soho.Model;

import java.util.ArrayList;
import java.util.List;

public class LoginDataModel{
    private ArrayList<LoginDataClientsModel> clients;

    public LoginDataModel(ArrayList<LoginDataClientsModel> clients) {
        this.clients = clients;
    }

    public ArrayList<LoginDataClientsModel> getClients(){
        return clients;
    }
}