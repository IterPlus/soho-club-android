package kh.com.soho.soho;

import android.os.Parcel;
import android.os.Parcelable;

public class HistoryDetailModel implements Parcelable {

    public String itemName;
    public String itemType;
    public String unitPrice;
    public String quantity;
    public String discountPercentage;
    public String discountAmount;
    public String itemCode;
    public String subTotal;

    public HistoryDetailModel(String itemName, String itemType, String unitPrice, String quantity, String discountPercentage, String discountAmount, String itemCode, String subTotal) {
        this.itemName = itemName;
        this.itemType = itemType;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
        this.discountPercentage = discountPercentage;
        this.discountAmount = discountAmount;
        this.itemCode = itemCode;
        this.subTotal = subTotal;
    }

    protected HistoryDetailModel(Parcel in) {
        itemName = in.readString();
        itemType = in.readString();
        unitPrice = in.readString();
        quantity = in.readString();
        discountPercentage = in.readString();
        discountAmount = in.readString();
        itemCode = in.readString();
        subTotal = in.readString();
    }

    public static final Creator<HistoryDetailModel> CREATOR = new Creator<HistoryDetailModel>() {
        @Override
        public HistoryDetailModel createFromParcel(Parcel in) {
            return new HistoryDetailModel(in);
        }

        @Override
        public HistoryDetailModel[] newArray(int size) {
            return new HistoryDetailModel[size];
        }
    };

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(itemName);
        dest.writeString(itemType);
        dest.writeString(unitPrice);
        dest.writeString(quantity);
        dest.writeString(discountPercentage);
        dest.writeString(discountAmount);
        dest.writeString(itemCode);
        dest.writeString(subTotal);
    }
}
