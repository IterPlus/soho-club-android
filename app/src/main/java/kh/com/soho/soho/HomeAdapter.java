package kh.com.soho.soho;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.WriterException;

import java.util.List;
import java.util.Objects;

import static kh.com.soho.soho.Ulti.encodeAsBitmap;

@SuppressWarnings("FieldCanBeLocal")
public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    Context context;
    private List<HomeModel> lists;
    private Boolean isCardFlip = true;

    HomeAdapter(List<HomeModel> lists, Context context) {
        this.lists = lists;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HomeModel cardList = lists.get(position);
        final Handler handler = new Handler();
        Bitmap bitmap = null;

        if (!Objects.equals(cardList.getPoint(), "null")) {
            holder.point.setText(cardList.getPoint());
        }

        try {
            bitmap = encodeAsBitmap(cardList.getCode());
        } catch (WriterException e) {
            e.printStackTrace();
        }

        holder.name.setText(cardList.getName());
        holder.code.setText(cardList.getCode());
        holder.imageView.setImageResource(cardList.getImageView());
        holder.qrCodeImageView.setImageBitmap(bitmap);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isCardFlip) {
                    isCardFlip = false;
                    holder.imageView.setImageResource(R.drawable.card_soho_back);

                    ObjectAnimator flip = ObjectAnimator.ofFloat(holder.imageView, "rotationY", 0f, 180f);

                    flip.setInterpolator(new AccelerateDecelerateInterpolator());
                    flip.setInterpolator(new DecelerateInterpolator());
                    holder.name.setVisibility(View.INVISIBLE);
                    holder.code.setVisibility(View.INVISIBLE);
                    holder.point.setVisibility(View.INVISIBLE);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            holder.qrCodeImageView.setVisibility(View.VISIBLE);
                        }
                    }, 200);

                    changeCameraDistance(holder.imageView);
                    flip.start();


                } else {
                    isCardFlip = true;
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                            holder.imageView.setImageResource(R.drawable.card_soho);
                            holder.name.setVisibility(View.VISIBLE);
                            holder.code.setVisibility(View.VISIBLE);
                            holder.point.setVisibility(View.VISIBLE);
                        }
                    }, 200);

                    holder.qrCodeImageView.setVisibility(View.INVISIBLE);
                    ObjectAnimator flip = ObjectAnimator.ofFloat(holder.imageView, "rotationY", 180f, 0f);
                    flip.setInterpolator(new AccelerateDecelerateInterpolator());
                    flip.setInterpolator(new DecelerateInterpolator());
                    changeCameraDistance(holder.imageView);
                    flip.start();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, code, point;
        ImageView imageView, qrCodeImageView;

        ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.row_home_name_textview);
            code = itemView.findViewById(R.id.row_home_code_textview);
            point = itemView.findViewById(R.id.row_home_point_textview);
            imageView = itemView.findViewById(R.id.row_home_card_image);
            qrCodeImageView = itemView.findViewById(R.id.row_home_qrcode_image);

        }
    }

    private void changeCameraDistance(ImageView imageView) {
        int distance = 8000;
        float scale = context.getResources().getDisplayMetrics().density * distance;
        imageView.setCameraDistance(scale);
    }
}
