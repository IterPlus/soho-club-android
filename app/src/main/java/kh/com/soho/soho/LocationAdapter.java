package kh.com.soho.soho;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static kh.com.soho.soho.OutletFragment.mGoogleMap;
import static kh.com.soho.soho.Ulti.mAll;
import static kh.com.soho.soho.Ulti.mBirkenStock;
import static kh.com.soho.soho.Ulti.mBonchon;
import static kh.com.soho.soho.Ulti.mCrocs;
import static kh.com.soho.soho.Ulti.mForME;
import static kh.com.soho.soho.Ulti.mHauteRack;
import static kh.com.soho.soho.Ulti.mHavaianas;
import static kh.com.soho.soho.Ulti.mINoRiJewels;
import static kh.com.soho.soho.Ulti.mMetallurgy;
import static kh.com.soho.soho.Ulti.mOxygen;
import static kh.com.soho.soho.Ulti.mPenshoppe;
import static kh.com.soho.soho.Ulti.mRokku;
import static kh.com.soho.soho.Ulti.mSanFrancisco;
import static kh.com.soho.soho.Ulti.mSprayWay;
import static kh.com.soho.soho.Ulti.mSwatch;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {
    private String[] outletList;
    private Context context;

    public LocationAdapter(String[] outletList, Context context) {
        this.outletList = outletList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_location, parent, false);
        return new LocationAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.outletTxt.setText(outletList[position]);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(outletList[position].equals(mCrocs)){
                    mGoogleMap.clear();
                    OutletFragment.initCrocs();
                }else if (outletList[position].equals(mForME)){
                    mGoogleMap.clear();
                    OutletFragment.initForMe();
                }else if(outletList[position].equals(mPenshoppe)){
                    mGoogleMap.clear();
                    OutletFragment.initPenshoppe();
                }else if(outletList[position].equals(mSwatch)) {
                    mGoogleMap.clear();
                    OutletFragment.initSwatch();
                }else if(outletList[position].equals(mBirkenStock)){
                    mGoogleMap.clear();
                    OutletFragment.initBirkenStock();
                }else if(outletList[position].equals(mHauteRack)){
                    mGoogleMap.clear();
                    OutletFragment.initHauteRack();
                }else if(outletList[position].equals(mHavaianas)){
                    mGoogleMap.clear();
                    OutletFragment.initHavaianas();
                }else if(outletList[position].equals(mINoRiJewels)){
                    mGoogleMap.clear();
                    OutletFragment.initInori();
                }else if(outletList[position].equals(mMetallurgy)){
                    mGoogleMap.clear();
                    OutletFragment.initMetallurgy();
                }else if(outletList[position].equals(mOxygen)){
                    mGoogleMap.clear();
                    OutletFragment.initOxygen();
                }else if(outletList[position].equals(mBonchon)){
                    mGoogleMap.clear();
                    OutletFragment.initBonchon();
                }else if(outletList[position].equals(mSprayWay)){
                    mGoogleMap.clear();
                    OutletFragment.initSprayWay();
                }else if(outletList[position].equals(mSanFrancisco)){
                    mGoogleMap.clear();
                    OutletFragment.initSanFrancisco();
                }else if(outletList[position].equals(mRokku)){
                    mGoogleMap.clear();
                    OutletFragment.initRokku();
                }else if(outletList[position].equals(mAll)){
                    mGoogleMap.clear();
                    OutletFragment.initAll();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return outletList.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView outletTxt;

        ViewHolder(View itemView) {
            super(itemView);
            outletTxt = itemView.findViewById(R.id.row_location_outlet);
        }
    }
}
