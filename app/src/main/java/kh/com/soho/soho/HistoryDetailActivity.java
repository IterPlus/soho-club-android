package kh.com.soho.soho;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.r0adkll.slidr.Slidr;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import kh.com.soho.soho.Custom.MyItemDecoration;
import kh.com.soho.soho.InterfaceService.InvoiceDetailService;
import kh.com.soho.soho.InterfaceService.LoadingHistoryService;
import kh.com.soho.soho.Model.HistoryPurchaseDetailModel;
import kh.com.soho.soho.Model.HistorySaleModel;
import kh.com.soho.soho.Model.InvoiceCollectionModel;
import kh.com.soho.soho.Model.InvoiceDetailModel;
import kh.com.soho.soho.Model.InvoiceItemModel;
import kh.com.soho.soho.Model.LoadingHistoryModel;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static kh.com.soho.soho.Ulti.checkInternetConnection;
import static kh.com.soho.soho.Ulti.showMessage;

@SuppressWarnings("FieldCanBeLocal")
public class HistoryDetailActivity extends AppCompatActivity {

    Toolbar mToolbar;
    HistoryModel historyList;
    List<HistoryDetailModel> historyDetailLists;
    HistoryDetailAdapter adapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    TextView invoiceNoTxt, cashierTxt, paymentDateTxt, outletName;
    ImageView outletLogoImg;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_detail);

        mToolbar = findViewById(R.id.act_history_detail_toolbar);
        invoiceNoTxt = findViewById(R.id.act_history_detail_invoice_no_txt);
        cashierTxt = findViewById(R.id.act_history_detail_cashier_txt);
        paymentDateTxt = findViewById(R.id.act_history_detail_date_txt);
        outletLogoImg = findViewById(R.id.act_history_detail_outlet_image);
        mRecyclerView = findViewById(R.id.act_history_detail_recycler);
        outletName = findViewById(R.id.act_history_detail_outlet_name_txt);
        progressBar = findViewById(R.id.act_history_detail_progress_bar);
        Slidr.attach(this);
        historyDetailLists = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        historyList = bundle.getParcelable("historyDetails");
        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.addItemDecoration(new MyItemDecoration());
        adapter = new HistoryDetailAdapter(historyDetailLists, this, historyList.payAmount);
        mRecyclerView.setAdapter(adapter);
        assert historyList != null;
        invoiceNoTxt.setText(historyList.saleId);
        outletName.setText(historyList.outlet);

        String[] outletName = historyList.getOutlet().split(" ");
        Log.d("historyList.getOu",""+ outletName[0]);
        Glide.with(this).load(getResources().getIdentifier(outletName[0].toLowerCase(), "drawable", this.getPackageName())).into(outletLogoImg);
        setmToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadHistoryDetail();
            }
        }, 500);
    }

    void loadHistoryDetail(){
        if (checkInternetConnection(Objects.requireNonNull(HistoryDetailActivity.this))) {
            OkHttpClient.Builder client = new OkHttpClient.Builder();
            client.connectTimeout(15, TimeUnit.SECONDS);
            client.readTimeout(15, TimeUnit.SECONDS);
            client.writeTimeout(15, TimeUnit.SECONDS);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Ulti.BaseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client.build())
                    .build();

            InvoiceDetailService service = retrofit.create(InvoiceDetailService.class);
            Call call = service.getInvoiceDetail(Ulti.urlXilnexSalesInVoice + historyList.saleId);
            call.enqueue(new Callback<InvoiceDetailModel>() {
                @Override
                public void onResponse(@NonNull Call<InvoiceDetailModel> call, @NonNull retrofit2.Response<InvoiceDetailModel> res) {
                    if (res.body() != null) {
                            Log.d("getInvoiceData","" + res.body().getInvoiceData().getInvoiceSale());
                        for (InvoiceItemModel item : res.body().getInvoiceData().getInvoiceSale().getInvoiceItems()){
                            String subTotal = String.valueOf(item.getSubtotal());
                            String itemCode = item.getItemCode();
                            String itemName = item.getItemName();
                            String quantity = String.valueOf(item.getQuantity());
                            String discountPercentage = String.valueOf(item.getDiscountPercentage());
                            String itemType = item.getItemType();
                            String discountAmount = String.valueOf(item.getDiscountAmount());
                            String unitPrice = String.valueOf(item.getUnitPrice());

                            historyDetailLists.add(new HistoryDetailModel(itemName, itemType, unitPrice, quantity, discountPercentage, discountAmount, itemCode, subTotal));
                            adapter.notifyDataSetChanged();
                        }

                        for (InvoiceCollectionModel collecction : res.body().getInvoiceData().getInvoiceSale().getCollections()){
                            String paymentDate = collecction.getPaymentDate();
                            String receivedBy = collecction.getReceivedBy();
                            String[] paymentDateSplit = paymentDate.split("T");
                            paymentDateTxt.setText(paymentDateSplit[0]);
                            cashierTxt.setText(receivedBy);
                        }

                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<InvoiceDetailModel> call, Throwable t) {
                    showMessage("No Internet Connection" + t, HistoryDetailActivity.this);
                }
            });
        }else{
            Objects.requireNonNull(HistoryDetailActivity.this).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showMessage("No Internet Connection", HistoryDetailActivity.this);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_card_detail, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setmToolbar() {
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}
