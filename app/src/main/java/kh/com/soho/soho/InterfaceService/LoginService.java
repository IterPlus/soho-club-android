package kh.com.soho.soho.InterfaceService;

import kh.com.soho.soho.Model.LoginModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface LoginService {

    @Headers({
            "token: 24U3HurDqGbf1oi7nZZTsYkIQDEk2ZYP48x/TWAJ004=",
            "dkey: VDEHwLkcwg6pi1P4GUekHpbc4hB8Oh7C"
    })

    @GET("clients/search?")
    Call<LoginModel> getUser(@Query("code")String phone);

}
