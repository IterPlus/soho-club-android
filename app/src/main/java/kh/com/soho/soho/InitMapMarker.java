package kh.com.soho.soho;

import com.google.android.gms.maps.GoogleMap;

import static kh.com.soho.soho.Ulti.*;

public class InitMapMarker {

    public static void initCrocsMarker(GoogleMap mMap){
//        GoogleMap googleMap, double latitude, double longitude, String title, String snippet, int iconResID
        OutletFragment.createMarker(mMap, mCrocsAeonMallLatitude, mCrocsAeonMallLongitude, mCrocsAeonMallTitle, mCrocsAeonMallAddress, Ulti.mCrocsPin);
        OutletFragment.createMarker(mMap, mCrocsTKAvenueLatitude, mCrocsTKAvenueLongitude, mCrocsTKAvenueTitle, mCrocsTKAvenueAddress, Ulti.mCrocsPin);
        OutletFragment.createMarker(mMap, mCrocsExchangeSquareLatitude, mCrocsExchangeSquareLongitude, mCrocsExchangeSquareTitle, mCrocsExchangeSquareAddress, Ulti.mCrocsPin);
        OutletFragment.createMarker(mMap, mCrocsSihanoukBlvdLatitude, mCrocsSihanoukBlvdLongitude, mCrocsSihanoukBlvdTitle, mCrocsSihanoukBlvdAddress, Ulti.mCrocsPin);
        OutletFragment.createMarker(mMap, mCrocsAEONMallSenSokLatitude, mCrocsAEONMallSenSokLongitude, mCrocsAEONMallSenSokTitle, mCrocsAEONMallSenSokAddress, Ulti.mCrocsPin);
//        OutletFragment.createMarker(mMap, mCrocsSoryaMallLatitude, mCrocsSoryaMallLongitude, mCrocsSoryaMallTitle, mCrocsSoryaMallAddress, Ulti.mCrocsPin);
    }

    static void initForMeMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mForMeAeonMallLatitude, mForMeAeonMallLongitude, mForMeAeonMallTitle, mForMeAeonMallAddress, Ulti.mForMePin);
    }

    static void initSwatchMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mSwatchAeonMallLatitude, mSwatchAeonMallLongitude, mSwatchAeonMallTitle, mSwatchAeonMallAddress, Ulti.mSwatchPin);
        OutletFragment.createMarker(mMap, mSwatchExchangeSquareLatitude, mSwatchExchangeSquareLongitude, mSwatchExchangeSquareTitle, mSwatchExchangeSquareAddress, Ulti.mSwatchPin);
        OutletFragment.createMarker(mMap, mSwatchAEONMallSenSokLatitude, mSwatchAEONMallSenSokLongitude, mSwatchAEONMallSenSokTitle, mSwatchAEONMallSenSokAddress, Ulti.mSwatchPin);
    }

    static void initPenshoppeMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mPenshoppeAeonMallLatitude, mPenshoppeAeonMallLongitude, mPenshoppeAeonMallTitle, mPenshoppeAeonMallAddress, Ulti.mPenshoppePin);
        OutletFragment.createMarker(mMap, mPenshoppeTKLatitude, mPenshoppeTKLongitude, mPenshoppeTKTitle, mPenshoppeTKAddress, Ulti.mPenshoppePin);
        OutletFragment.createMarker(mMap, mPenshoppeLuckyMallLatitude, mPenshoppeLuckyMallLongitude, mPenshoppeLuckyMallTitle, mPenshoppeLuckyMallAddress, Ulti.mPenshoppePin);
        OutletFragment.createMarker(mMap, mPenshoppeAeonMallSenSokLatitude, mPenshoppeAeonMallSenSokLongitude, mPenshoppeAeonMallSenSokTitle, mPenshoppeAeonMallSenSokAddress, Ulti.mPenshoppePin);
    }

    static void initBirkenStockMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mBirkenstockAEONMallSenSokLatitude, mBirkenstockAEONMallSenSokLongitude, mBirkenstockAEONMallSenSokTitle, mBirkenstockAEONMallSenSokAddress, Ulti.mBirkenStockPin);
    }

    static void initHauteRackMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mHauteRackAeonSenSokLatitude, mHauteRackAeonSenSokLongitude, mHauteRackAeonSenSokTitle, mHauteRackAeonSenSokAddress, Ulti.mHauteRackPin);
    }

    static void initHavaianasMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mHavaianasAEONMallLatitude, mHavaianasAEONMallLongitude, mHavaianasAEONMallTitle, mHavaianasAEONMallAdddress, Ulti.mHavaianasPin);
        OutletFragment.createMarker(mMap, mHavaianasPP63Latitude, mHavaianasPP63Longitude, mHavaianasPP63Title, mHavaianasPP63Adddress, Ulti.mHavaianasPin);
        OutletFragment.createMarker(mMap, mHavaianasBKKLatitude, mHavaianasBKKLongitude, mHavaianasBKKTitle, mHavaianasBKKAddress, Ulti.mHavaianasPin);
        OutletFragment.createMarker(mMap, mHavaianasTKAvenueLatitude, mHavaianasTKAvenueLongitude, mHavaianasTKAvenueTitle, mHavaianasTKAvenueAddress, Ulti.mHavaianasPin);
        OutletFragment.createMarker(mMap, mHavaianasKingsRoadAngkorLatitude, mHavaianasKingsRoadAngkorLongitude, mHavaianasKingsRoadAngkorTitle, mHavaianasKingsRoadAngkorAddress, Ulti.mHavaianasPin);
        OutletFragment.createMarker(mMap, mHavaianasLuckyMallLatitude, mHavaianasLuckyMallLongitude, mHavaianasLuckyMallTitle, mHavaianasLuckyMallAddress, Ulti.mHavaianasPin);
        OutletFragment.createMarker(mMap, mHavaianasAngkorTicketCenterLatitude, mHavaianasAngkorTicketCenterLongitude, mHavaianasAngkorTicketCenterTitle, mHavaianasAngkorTicketCenterAddress, Ulti.mHavaianasPin);
        OutletFragment.createMarker(mMap, mHavaianasExchangeSquareMallLatitude, mHavaianasExchangeSquareMallLongitude, mHavaianasExchangeSquareMallTitle, mHavaianasExchangeSquareMallAddress, Ulti.mHavaianasPin);
        OutletFragment.createMarker(mMap, mHavaianasSoryaCenterPointLatitude, mHavaianasSoryaCenterPointLongitude, mHavaianasSoryaCenterPointTitle, mHavaianasSoryaCenterPointAddress, Ulti.mHavaianasPin);
        OutletFragment.createMarker(mMap, mHavaianasPhnomPenhAirportLatitude, mHavaianasPhnomPenhAirportLongitude, mHavaianasPhnomPenhAirportTitle, mHavaianasPhnomPenhAirportAddress, Ulti.mHavaianasPin);
        OutletFragment.createMarker(mMap, mHavaianasAEONMallSenSokLatitude, mHavaianasAEONMallLongitude, mHavaianasAEONMallSenSokTitle, mHavaianasAEONMallSenSokAddress, Ulti.mHavaianasPin);

    }

    static void initInoriMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mINoRiJewelsAEONMallSenSokLatitude, mINoRiJewelsAEONMallSenSokLongitude, mINoRiJewelsAEONMallSenSokTitle, mINoRiJewelsAEONMallSenSokAddress, Ulti.mInoriPin);
    }

    static void initMetallurgyMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mMetallurgyAEONMallLatitude, mMetallurgyAEONMallLongitude, mMetallurgyAEONMallTitle, mMetallurgyAEONMallAddress, Ulti.mMetallurgyPin);
        OutletFragment.createMarker(mMap, mMetallurgyTKAvenueLatitude, mMetallurgyTKAvenueLongitude, mMetallurgyTKAvenueTitle, mMetallurgyTKAvenueAddress, Ulti.mMetallurgyPin);
    }

    static void initOxygenMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mOxygenAEONMallSenSokLatitude, mOxygenAEONMallSenSokLongitude, mOxygenAEONMallSenSokTitle, mOxygenAEONMallSenSokAddress, Ulti.mOxygenPin);
    }

    static void initBonchonMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mBonchonPhnomPenhCenterLatitude, mBonchonPhnomPenhCenterLongitude, mBonchonPhnomPenhCenterTitle, mBonchonPhnomPenhCenterAddress, Ulti.mBonChonPin);
        OutletFragment.createMarker(mMap, mBonchonAeonMallLatitude, mBonchonAeonMallLongitude, mBonchonAeonMallTitle, mBonchonAeonMallAddress, Ulti.mBonChonPin);
        OutletFragment.createMarker(mMap, mBonchonBKKLatitude, mBonchonBKKLongitude, mBonchonBKKTitle, mBonchonBKKAddress, Ulti.mBonChonPin);
        OutletFragment.createMarker(mMap, mBonchonRiverSideLatitude, mBonchonRiverSideLongitude, mBonchonRiverSideTitle, mBonchonRiverSideAddress, Ulti.mBonChonPin);
        OutletFragment.createMarker(mMap, mBonchonTKLongitude, mBonchonTKLatitude, mBonchonTKTitle, mBonchonTKAddress, Ulti.mBonChonPin);
        OutletFragment.createMarker(mMap, mBonchonAeonMaxValueExpressLatitude, mBonchonAeonMaxValueExpressLongitude, mBonchonAeonMaxValueExpressTitle, mBonchonAeonMaxValueExpressAddress, Ulti.mBonChonPin);
        OutletFragment.createMarker(mMap, mBonchonExchangeSquareLatitude, mBonchonExchangeSquareLongitude, mBonchonExchangeSquareTitle, mBonchonExchangeSquareAddress, Ulti.mBonChonPin);
        OutletFragment.createMarker(mMap, mBonchonIFLLatitude, mBonchonIFLLongitude, mBonchonIFLTitle, mBonchonIFLAddress, Ulti.mBonChonPin);
        OutletFragment.createMarker(mMap, mBonchonAttwoodLatitude, mBonchonAttwoodLongitude, mBonchonAttwoodTitle, mBonchonAttwoodAddress, Ulti.mBonChonPin);
        OutletFragment.createMarker(mMap, mBonchonSovannaLatitude, mBonchonSovannakLongitude, mBonchonSovannaTitle, mBonchonSovannaAddress, Ulti.mBonChonPin);
        OutletFragment.createMarker(mMap, mBonchonStoengMeancheyLatitude, mBonchonStoengMeancheyLongitude, mBonchonStoengMeancheyTitle, mBonchonStoengMeancheyAddress, Ulti.mBonChonPin);
        OutletFragment.createMarker(mMap, mBonchonSoryaMallLatitude, mBonchonSoryaMallLongitude, mBonchonSoryaMallTitle, mBonchonSoryaMallAddress, Ulti.mBonChonPin);
        OutletFragment.createMarker(mMap, mBonchonAeonMallSenSokLatitude, mBonchonAeonMallSenSokLongitude, mBonchonAeonMallSenSokCityTitle, mBonchonAeonMallSenSokCityAddress, Ulti.mBonChonPin);

    }

    static void initSprayWayMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mSprayWayAEONMallSenSokLatitude, mSprayWayAEONMallSenSokLongitude, mSprayWayAEONMallSenSokTitle, mSprayWayAEONMallSenSokAddress, Ulti.mSprayWayPin);
    }

    static void initSanFranciscoMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mSanFranciscoExchangeSquareLatitude, mSanFranciscoExchangeSquareLongitude, mSanFranciscoExchangeSquareTitle, mSanFranciscoExchangeSquareAddress, Ulti.mSanFranciscoPin);
        OutletFragment.createMarker(mMap, mPenshoppeAeonMallSenSokLatitude, mPenshoppeAeonMallSenSokLongitude, mPenshoppeAeonMallSenSokTitle, mPenshoppeAeonMallSenSokAddress, Ulti.mSanFranciscoPin);
    }

    static void initRokkuMarker(GoogleMap mMap){
        OutletFragment.createMarker(mMap, mRokkuLatitude, mRokkuLongitude, mRokkuTitle, mRokkuAddress, Ulti.mRokkuPin);
    }

}
