package kh.com.soho.soho;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder>{
    private List<HistoryModel> historyList;
    private Context context;

    public HistoryAdapter(List<HistoryModel> historyList, Context context) {
        this.historyList = historyList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_history, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final HistoryModel list = historyList.get(position);

        holder.outletTxt.setText(list.outlet);
        holder.invoiceNoTxt.setText(list.saleId);
        holder.amountTxt.setText("$" + list.amount);
        holder.paidAmountTxt.setText("$" + list.payAmount);
        holder.saleDateTxt.setText(list.saleDate);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HistoryDetailActivity.class);
                intent.putExtra("historyDetails", list);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView outletTxt, invoiceNoTxt, amountTxt, paidAmountTxt, saleDateTxt;

        ViewHolder(View itemView) {
            super(itemView);
            outletTxt = itemView.findViewById(R.id.act_history_row_outlet_txt);
            invoiceNoTxt = itemView.findViewById(R.id.act_history_row_invoice_txt);
            amountTxt = itemView.findViewById(R.id.act_history_row_amount_txt);
            paidAmountTxt = itemView.findViewById(R.id.act_history_row_paid_amount_txt);
            saleDateTxt = itemView.findViewById(R.id.act_history_row_sale_date_txt);
        }
    }
}
