package kh.com.soho.soho;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import kh.com.soho.soho.InterfaceService.LoadingHistoryService;
import kh.com.soho.soho.InterfaceService.LoginService;
import kh.com.soho.soho.Model.HistoryPurchaseDetailModel;
import kh.com.soho.soho.Model.HistorySaleModel;
import kh.com.soho.soho.Model.LoadingHistoryModel;
import kh.com.soho.soho.Model.LoginDataClientsModel;
import kh.com.soho.soho.Model.LoginModel;
import kh.com.soho.soho.SQLite.DatabaseHelper;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static kh.com.soho.soho.Ulti.checkInternetConnection;
import static kh.com.soho.soho.Ulti.showMessage;

public class LaunchScreenActivity extends AppCompatActivity {
    HomeModel list;
    List<HistoryModel> historyLists;
    DatabaseHelper databaseHelper;
    ImageView imageView;
    OkHttpClient.Builder client = new OkHttpClient.Builder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_screen);
        imageView = findViewById(R.id.act_launch_image);
        databaseHelper = new DatabaseHelper(this);



                //Check login Token
                Cursor cursor = databaseHelper.queryToken();
                if (cursor.moveToNext()) {
                    client.connectTimeout(15, TimeUnit.SECONDS);
                    client.readTimeout(15, TimeUnit.SECONDS);
                    client.writeTimeout(15, TimeUnit.SECONDS);

                    loadData();
                    loadHistory();
                    historyLists = new ArrayList<>();
                }else{

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(LaunchScreenActivity.this, WelcomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }, 2000);

                }

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    public void loadData() {
        Cursor cursor = databaseHelper.queryAll();
        while (cursor.moveToNext()) {
            final String id = cursor.getString(0);
            final String name = cursor.getString(1);
            final String dob = cursor.getString(2);
//            String email = cursor.getString(3);
            final String phone = cursor.getString(4);
            final String point = cursor.getString(5);
            final String outlet = cursor.getString(6);
            final String token = cursor.getString(7);
            final String dkey = cursor.getString(8);
            final String serial = cursor.getString(9);
            final int cardimage = cursor.getInt(10);
            final int logo = cursor.getInt(11);

            list = new HomeModel(id, name, serial, point, phone, dob, token, dkey, outlet, logo, cardimage);
        }
    }

    void updatePoint(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Ulti.BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();

        LoginService service = retrofit.create(LoginService.class);
        Call call = service.getUser(list.getPhone());
        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(@NonNull Call<LoginModel> call, @NonNull Response<LoginModel> response) {
                Log.d("response.code().", "" + response.code());
                if (response.body() != null && response.body().getStatus().equals("SuccessQuery")) {
                    for (LoginDataClientsModel data : response.body().getLoginData().getClients()){
                        Log.d("response.body().", "" + data.getUserName());

                        String alternateLookup = data.getUserAlterNateLookup();
                        int pointOrigin = data.getUserPointValue();
                        String pointValue = String.valueOf(pointOrigin);

                        if (!pointValue.equals(list.getPoint())) {
                            databaseHelper.updatePoint(pointValue, alternateLookup);
                        }else{
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    showMessage("Can not add this card because some fill is missing, Please check with post system.\nThank You!", LaunchScreenActivity.this);
                                }
                            });
                        }
                    }

                    Intent intent = new Intent(LaunchScreenActivity.this, TabBarActivity.class);
                    intent.putExtra("dbquery", list);
                    intent.putExtra("db_history", (Serializable) historyLists);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                showMessage("No Internet Connection" + t, LaunchScreenActivity.this);
            }
        });
    }

    void loadHistory(){

        if (checkInternetConnection(Objects.requireNonNull(LaunchScreenActivity.this))) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Ulti.BaseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client.build())
                    .build();

            LoadingHistoryService service = retrofit.create(LoadingHistoryService.class);
            Call call = service.getHistorySale(Ulti.urlXilnexSaleHistory(String.valueOf(list.getId())));
            call.enqueue(new Callback<LoadingHistoryModel>() {
                @Override
                public void onResponse(@NonNull Call<LoadingHistoryModel> call, @NonNull Response<LoadingHistoryModel> res) {
                    if (res.body() != null) {
                        // get Purchase History
                        for (HistorySaleModel purchase : res.body().getHistoryData().getPurchaseHistories()){
                            // get sales Detail
                            for (HistoryPurchaseDetailModel sale : purchase.getSales()){
                                String saleId = String.valueOf(sale.getSalesId());
                                String amount = String.valueOf(sale.getSaleAmount());
                                String paidAmount = String.valueOf(sale.getSalePaidAmount());
                                String salesDate = sale.getSaleDate();
                                String[] splitSaleDate = salesDate.split("T00");

                                historyLists.add(new HistoryModel(purchase.getOutlet(), saleId, amount, paidAmount, splitSaleDate[0].trim(), list.getLogo()));
                            }
                        }

                        updatePoint();
                    }
                }

                @Override
                public void onFailure(Call<LoadingHistoryModel> call, Throwable t) {
                    showMessage("No Internet Connection" + t, LaunchScreenActivity.this);
                }
            });

        }else{
            Objects.requireNonNull(LaunchScreenActivity.this).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showMessage("No Internet Connection", LaunchScreenActivity.this);
                }
            });
        }
    }
}
