package kh.com.soho.soho;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ProgressBar;

import java.util.ArrayList;

import kh.com.soho.soho.Adapter.PromotionAdapter;
import kh.com.soho.soho.Custom.MyItemDecoration;

public class PromotionFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    WebView webView;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    PromotionAdapter adapter;
    public ArrayList<String> promo = new ArrayList<>();
    private GridLayoutManager mGridLayoutManager;
    private OnFragmentInteractionListener mListener;

    public static PromotionFragment newInstance(String param1, String param2) {
        PromotionFragment fragment = new PromotionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        Log.d("onCreate","onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_promotion, container, false);
        recyclerView = v.findViewById(R.id.fragment_promotion_recycler);
        initPromo();
        mGridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mGridLayoutManager);
        recyclerView.addItemDecoration(new MyItemDecoration());
        adapter = new PromotionAdapter(promo, getActivity());
        recyclerView.setAdapter(adapter);
        return v;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("super.onPause()","super.onPause()");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("super.onStart()","super.onStart()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("super.onStop()","super.onStop()");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void initPromo(){
        promo.add("pro_swatch");
        promo.add("pro_birkenstock");
        promo.add("pro_oxgn");
        promo.add("pro_metallurgy");
        promo.add("pro_forme");
        promo.add("pro_inori");
        promo.add("pro_bonchon");
        promo.add("pro_crocs");
        promo.add("pro_hauterack");
        promo.add("pro_havaianas");
        promo.add("pro_penshoppe");
        promo.add("pro_rokku");
        promo.add("pro_sanfrancisco");
        promo.add("pro_sprayway");

    }
}
