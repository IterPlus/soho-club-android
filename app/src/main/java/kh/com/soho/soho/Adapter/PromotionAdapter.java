package kh.com.soho.soho.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import kh.com.soho.soho.PopUpWebsiteActivity;
import kh.com.soho.soho.R;

public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.ViewHolder>{
    private ArrayList<String> promotions;
    private Context context;

    public PromotionAdapter(ArrayList<String> promotions, Context context) {
        this.promotions = promotions;
        this.context = context;
    }

    @NonNull
    @Override
    public PromotionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_promotion, parent, false);
        GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) v.getLayoutParams();
        lp.height = parent.getMeasuredHeight() / 4;
        v.setLayoutParams(lp);
        return new PromotionAdapter.ViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull PromotionAdapter.ViewHolder holder, int position) {
        final String imageName = promotions.get(position);

        Log.d("imageName", "" + imageName);
        int resID = context.getResources().getIdentifier( imageName , "drawable", context.getPackageName());
        holder.imageView.setImageDrawable(context.getDrawable(resID));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PopUpWebsiteActivity.class);
                intent.putExtra("promotion", imageName);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return promotions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.act_promotion_row_image);
        }
    }
}
