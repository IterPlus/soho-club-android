package kh.com.soho.soho;

import android.os.Parcel;
import android.os.Parcelable;

public class HomeModel implements Parcelable{
    private String id;
    private String name;
    private String code;
    private String point;
    private String phone;
    private String dob;
    private String token;
    private String dkey;
    private String outlet;
    private int logo;
    private int imageView;

    public HomeModel(String id, String name, String code, String point, String phone, String dob, String token, String dkey, String outlet, int logo, int imageView) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.point = point;
        this.phone = phone;
        this.dob = dob;
        this.token = token;
        this.dkey = dkey;
        this.outlet = outlet;
        this.logo = logo;
        this.imageView = imageView;
    }

    protected HomeModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        code = in.readString();
        point = in.readString();
        phone = in.readString();
        dob = in.readString();
        token = in.readString();
        dkey = in.readString();
        outlet = in.readString();
        logo = in.readInt();
        imageView = in.readInt();
    }

    public static final Creator<HomeModel> CREATOR = new Creator<HomeModel>() {
        @Override
        public HomeModel createFromParcel(Parcel in) {
            return new HomeModel(in);
        }

        @Override
        public HomeModel[] newArray(int size) {
            return new HomeModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getPoint() {
        return point;
    }

    public String getPhone() {
        return phone;
    }

    public String getDob() {
        return dob;
    }

    public String getToken() {
        return token;
    }

    public String getDkey() {
        return dkey;
    }

    public String getOutlet() {
        return outlet;
    }

    public int getLogo() {
        return logo;
    }

    public int getImageView() {
        return imageView;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setDkey(String dkey) {
        this.dkey = dkey;
    }

    public void setOutlet(String outlet) {
        this.outlet = outlet;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public void setImageView(int imageView) {
        this.imageView = imageView;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(code);
        dest.writeString(point);
        dest.writeString(phone);
        dest.writeString(dob);
        dest.writeString(token);
        dest.writeString(dkey);
        dest.writeString(outlet);
        dest.writeInt(logo);
        dest.writeInt(imageView);
    }
}
