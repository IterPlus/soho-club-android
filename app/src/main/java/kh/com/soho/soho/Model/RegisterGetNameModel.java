package kh.com.soho.soho.Model;

public class RegisterGetNameModel {
    private String first_name = "";
    private String last_name = "";

    public String getFirstName(){
        return first_name;
    }

    public String getLastName(){
        return last_name;
    }
}
