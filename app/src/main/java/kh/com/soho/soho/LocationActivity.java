package kh.com.soho.soho;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import kh.com.soho.soho.Custom.MyItemDecoration;

import static kh.com.soho.soho.Ulti.mAll;
import static kh.com.soho.soho.Ulti.mCrocs;
import static kh.com.soho.soho.Ulti.mCrocsAEONMallSenSokAddress;
import static kh.com.soho.soho.Ulti.mCrocsAEONMallSenSokLatitude;
import static kh.com.soho.soho.Ulti.mCrocsAEONMallSenSokLongitude;
import static kh.com.soho.soho.Ulti.mCrocsAEONMallSenSokTitle;
import static kh.com.soho.soho.Ulti.mCrocsAeonMallAddress;
import static kh.com.soho.soho.Ulti.mCrocsAeonMallLatitude;
import static kh.com.soho.soho.Ulti.mCrocsAeonMallLongitude;
import static kh.com.soho.soho.Ulti.mCrocsAeonMallTitle;
import static kh.com.soho.soho.Ulti.mCrocsExchangeSquareAddress;
import static kh.com.soho.soho.Ulti.mCrocsExchangeSquareLatitude;
import static kh.com.soho.soho.Ulti.mCrocsExchangeSquareLongitude;
import static kh.com.soho.soho.Ulti.mCrocsExchangeSquareTitle;
import static kh.com.soho.soho.Ulti.mCrocsPin;
import static kh.com.soho.soho.Ulti.mCrocsSihanoukBlvdAddress;
import static kh.com.soho.soho.Ulti.mCrocsSihanoukBlvdLatitude;
import static kh.com.soho.soho.Ulti.mCrocsSihanoukBlvdLongitude;
import static kh.com.soho.soho.Ulti.mCrocsSihanoukBlvdTitle;
import static kh.com.soho.soho.Ulti.mCrocsTKAvenueAddress;
import static kh.com.soho.soho.Ulti.mCrocsTKAvenueLatitude;
import static kh.com.soho.soho.Ulti.mCrocsTKAvenueLongitude;
import static kh.com.soho.soho.Ulti.mCrocsTKAvenueTitle;
import static kh.com.soho.soho.Ulti.mForME;
import static kh.com.soho.soho.Ulti.mForMePin;
import static kh.com.soho.soho.Ulti.mPenshoppe;
import static kh.com.soho.soho.Ulti.mSwatch;
import static kh.com.soho.soho.Ulti.mSwatchPin;

@SuppressWarnings("FieldCanBeLocal")
public class LocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    static GoogleMap mMap;
    static Marker marker;
    private LocationManager mLocationManager;
    private double latitude, longitude;
    List<AnnotationModel> outletAnnotation;
    LocationListener mLocationListenner;
    LocationAdapter adapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    String[] outlets = new String[]{mCrocs, mForME, mPenshoppe, mSwatch, mAll};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        outletAnnotation = new ArrayList<>();

        mRecyclerView = findViewById(R.id.act_location_recycler);
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.addItemDecoration(new MyItemDecoration());
        adapter = new LocationAdapter(outlets, LocationActivity.this);
        mRecyclerView.setAdapter(adapter);

        statusCheck();
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mLocationListenner = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    drawMarker(location);
                    mLocationManager.removeUpdates(mLocationListenner);
                } else {
                    Log.d("Location is null", "");
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_frm));
        mapFragment.getMapAsync(this);
        getLocation();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        initCrocsMarker();
        LatLng currentLcoation = new LatLng(latitude, longitude);
        Log.d("currentLcoation",""+ currentLcoation);
        LatLng cityMall = new LatLng(11.558627, 104.909584);

//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLcoation, 18), 5000, null);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cityMall, 12), 4000, null);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    static void initCrocsMarker(){
        createMarker(mMap, mCrocsAeonMallLatitude, mCrocsAeonMallLongitude, mCrocsAeonMallTitle, mCrocsAeonMallAddress, Ulti.mCrocsPin);
        createMarker(mMap, mCrocsTKAvenueLatitude, mCrocsTKAvenueLongitude, mCrocsTKAvenueTitle, mCrocsTKAvenueAddress, Ulti.mCrocsPin);
        createMarker(mMap, mCrocsExchangeSquareLatitude, mCrocsExchangeSquareLongitude, mCrocsExchangeSquareTitle, mCrocsExchangeSquareAddress, Ulti.mCrocsPin);
        createMarker(mMap, mCrocsSihanoukBlvdLatitude, mCrocsSihanoukBlvdLongitude, mCrocsSihanoukBlvdTitle, mCrocsSihanoukBlvdAddress, Ulti.mCrocsPin);
        createMarker(mMap, mCrocsAEONMallSenSokLatitude, mCrocsAEONMallSenSokLongitude, mCrocsAEONMallSenSokTitle, mCrocsAEONMallSenSokAddress, Ulti.mCrocsPin);
//        createMarker(mMap, mCrocsSoryaMallLatitude, mCrocsSoryaMallLongitude, mCrocsSoryaMallTitle, mCrocsSoryaMallAddress, Ulti.mCrocsPin);
    }

    public static void createMarker(GoogleMap googleMap, double latitude, double longitude, String title, String snippet, int iconResID) {

        marker = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));

    }

    private void drawMarker(Location location) {
        if (mMap != null) {
            mMap.clear();
            LatLng gps = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.addMarker(new MarkerOptions()
                    .position(gps)
                    .title("Current Position"));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(gps, 5));
        }

    }

    private void getLocation(){
        if (ActivityCompat.checkSelfPermission(LocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (LocationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(LocationActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
            Location location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            Location location1 = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = mLocationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();

            } else  if (location1 != null) {
                latitude = location1.getLatitude();
                longitude = location1.getLongitude();

            } else  if (location2 != null) {
                latitude = location2.getLatitude();
                longitude = location2.getLongitude();
            }else{

                Toast.makeText(this,"Unble to Trace your location",Toast.LENGTH_SHORT).show();
            }
        }
    }
    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        assert manager != null;
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
