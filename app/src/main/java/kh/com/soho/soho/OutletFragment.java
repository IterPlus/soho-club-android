package kh.com.soho.soho;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import kh.com.soho.soho.Custom.MyItemDecoration;

import static kh.com.soho.soho.Ulti.mAll;
import static kh.com.soho.soho.Ulti.mBirkenStock;
import static kh.com.soho.soho.Ulti.mBonchon;
import static kh.com.soho.soho.Ulti.mCrocs;
import static kh.com.soho.soho.Ulti.mForME;
import static kh.com.soho.soho.Ulti.mHauteRack;
import static kh.com.soho.soho.Ulti.mHavaianas;
import static kh.com.soho.soho.Ulti.mINoRiJewels;
import static kh.com.soho.soho.Ulti.mMetallurgy;
import static kh.com.soho.soho.Ulti.mOxygen;
import static kh.com.soho.soho.Ulti.mPenshoppe;
import static kh.com.soho.soho.Ulti.mRokku;
import static kh.com.soho.soho.Ulti.mSanFrancisco;
import static kh.com.soho.soho.Ulti.mSprayWay;
import static kh.com.soho.soho.Ulti.mSwatch;

public class OutletFragment extends Fragment implements OnMapReadyCallback {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    static GoogleMap mGoogleMap;
    static Marker marker;
    private LocationManager mLocationManager;
    private double latitude, longitude;
    List<AnnotationModel> outletAnnotation;
    LocationListener mLocationListenner;
    LocationAdapter adapter;
    public ImageView storeImage;
    public TextView storeAddressTV;
    public Button closeBtn;
    public RelativeLayout detailRelativeLayout;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private SupportMapFragment mapFragment;
    String[] outlets = new String[]{mCrocs, mForME, mPenshoppe, mSwatch, mBirkenStock, mHauteRack, mHavaianas, mINoRiJewels, mMetallurgy, mOxygen, mBonchon, mSprayWay, mSanFrancisco, mRokku, mAll};

    private OnFragmentInteractionListener mListener;

    public OutletFragment() {
        // Required empty public constructor
    }

    public static OutletFragment newInstance(String param1, String param2) {
        OutletFragment fragment = new OutletFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_outlet, container, false);
        outletAnnotation = new ArrayList<>();
        detailRelativeLayout = v.findViewById(R.id.fragment_outlet_store_parent);
        storeImage = v.findViewById(R.id.fragment_outlet_store_image);
        storeAddressTV = v.findViewById(R.id.fragment_outlet_store_address_tv);
        closeBtn = v.findViewById(R.id.fragment_outlet_close_btn);
        mRecyclerView = v.findViewById(R.id.fragment_outlet_recycler);
        mLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.addItemDecoration(new MyItemDecoration());
        adapter = new LocationAdapter(outlets, getContext());
        mRecyclerView.setAdapter(adapter);
        statusCheck();

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailRelativeLayout.setVisibility(View.INVISIBLE);
            }
        });

        mLocationManager = (LocationManager) Objects.requireNonNull(getActivity()).getSystemService(Context.LOCATION_SERVICE);
        mLocationListenner = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    Log.d("location", "" + location.getLatitude() + location.getLongitude());
                    drawMarker(location);
                    mLocationManager.removeUpdates(mLocationListenner);
                } else {
                    Log.d("Location is null", "");
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            mapFragment.getMapAsync(this);
        }

        getLocation();
        getChildFragmentManager().beginTransaction().replace(R.id.fragment_outlet_map, mapFragment).commit();

        return v;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //TODO
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;
        Log.d("googleMap","" + googleMap);
        initAll();
        LatLng currentLcoation = new LatLng(latitude, longitude);
//        LatLng cityMall = new LatLng(11.558627, 104.909584);

        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLcoation, 12), 4000, null);
        if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);

        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Log.d("Marker::", "" + marker.getTitle());
                String title = marker.getTitle().toLowerCase();
                String replacing = title.replace(" ", "_");
                detailRelativeLayout.setVisibility(View.VISIBLE);

                int resID = getResources().getIdentifier(replacing , "drawable", getActivity().getPackageName());
                Glide.with(getContext()).load(resID).apply(RequestOptions.placeholderOf(R.drawable.birkenstock_aeon_mall_sen_sok_city)).into(storeImage);
//                storeImage.setImageResource(resID);

                storeAddressTV.setText(marker.getSnippet());
                return false;
            }
        });
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) Objects.requireNonNull(getActivity()).getSystemService(Context.LOCATION_SERVICE);

        assert manager != null;
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void getLocation(){
        if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            Location location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location location1 = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location location2 = mLocationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            } else  if (location1 != null) {
                latitude = location1.getLatitude();
                longitude = location1.getLongitude();
            } else  if (location2 != null) {
                latitude = location2.getLatitude();
                longitude = location2.getLongitude();
            }else{
                Toast.makeText(getContext(), "Unable to Trace your location",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void drawMarker(Location location) {
        if (mGoogleMap != null) {
            mGoogleMap.clear();
            LatLng gps = new LatLng(location.getLatitude(), location.getLongitude());
            mGoogleMap.addMarker(new MarkerOptions()
                    .position(gps)
                    .title("Current Position"));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(gps, 5));
        }

    }

    static void initCrocs(){
        InitMapMarker.initCrocsMarker(mGoogleMap);
    }

    static void initForMe(){
        InitMapMarker.initForMeMarker(mGoogleMap);
    }

    static void initSwatch(){
        InitMapMarker.initSwatchMarker(mGoogleMap);
    }

    static void initPenshoppe(){
        InitMapMarker.initPenshoppeMarker(mGoogleMap);
    }

    static void initBirkenStock(){
        InitMapMarker.initBirkenStockMarker(mGoogleMap);
    }

    static void initHauteRack(){
        InitMapMarker.initHauteRackMarker(mGoogleMap);
    }

    static void initHavaianas(){
        InitMapMarker.initHavaianasMarker(mGoogleMap);
    }

    static void initMetallurgy(){
        InitMapMarker.initMetallurgyMarker(mGoogleMap);
    }

    static void initInori(){
        InitMapMarker.initInoriMarker(mGoogleMap);
    }

    static void initOxygen(){
        InitMapMarker.initOxygenMarker(mGoogleMap);
    }

    static void initBonchon(){
        InitMapMarker.initBonchonMarker(mGoogleMap);
    }

    static void initSprayWay(){
        InitMapMarker.initSprayWayMarker(mGoogleMap);
    }

    static void initSanFrancisco(){
        InitMapMarker.initSanFranciscoMarker(mGoogleMap);
    }

    static void initRokku(){
        InitMapMarker.initRokkuMarker(mGoogleMap);
    }

    static void initAll(){
        InitMapMarker.initCrocsMarker(mGoogleMap);
        InitMapMarker.initForMeMarker(mGoogleMap);
        InitMapMarker.initSwatchMarker(mGoogleMap);
        InitMapMarker.initPenshoppeMarker(mGoogleMap);
        InitMapMarker.initBirkenStockMarker(mGoogleMap);
        InitMapMarker.initHauteRackMarker(mGoogleMap);
        InitMapMarker.initHavaianasMarker(mGoogleMap);
        InitMapMarker.initMetallurgyMarker(mGoogleMap);
        InitMapMarker.initInoriMarker(mGoogleMap);
        InitMapMarker.initBonchonMarker(mGoogleMap);
        InitMapMarker.initSprayWayMarker(mGoogleMap);
        InitMapMarker.initSanFranciscoMarker(mGoogleMap);
        InitMapMarker.initRokkuMarker(mGoogleMap);
    }

    static void createMarker(GoogleMap googleMap, double latitude, double longitude, String title, String snippet, int iconResID) {

        marker= googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));

    }
}
