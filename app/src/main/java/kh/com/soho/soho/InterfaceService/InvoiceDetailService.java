package kh.com.soho.soho.InterfaceService;

import kh.com.soho.soho.Model.InvoiceDetailModel;
import kh.com.soho.soho.Model.LoadingHistoryModel;
import kh.com.soho.soho.Model.RegisterUserModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Url;

public interface InvoiceDetailService {

    @Headers({
            "token: 24U3HurDqGbf1oi7nZZTsYkIQDEk2ZYP48x/TWAJ004=",
            "dkey: VDEHwLkcwg6pi1P4GUekHpbc4hB8Oh7C"
    })

    @GET
    Call<InvoiceDetailModel> getInvoiceDetail(@Url String url);
}
