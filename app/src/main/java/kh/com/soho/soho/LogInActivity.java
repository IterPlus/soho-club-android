package kh.com.soho.soho;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.r0adkll.slidr.Slidr;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import kh.com.soho.soho.InterfaceService.LoginService;
import kh.com.soho.soho.Model.LoginDataClientsModel;
import kh.com.soho.soho.Model.LoginModel;
import kh.com.soho.soho.SQLite.DatabaseHelper;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static kh.com.soho.soho.Ulti.checkInternetConnection;
import static kh.com.soho.soho.Ulti.showMessage;

public class LogInActivity extends AppCompatActivity {

    Toolbar mToolbar;
    EditText phoneNumberTV;
    ProgressBar mProgressBar;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        phoneNumberTV = findViewById(R.id.act_login_phone_no_edittext);
        mToolbar = findViewById(R.id.act_card_log_in_toolbar);
        mProgressBar = findViewById(R.id.act_login_progressBar);
        databaseHelper = new DatabaseHelper(this);
        setmToolbar();

        Slidr.attach(this);
        Ulti.setupUI(findViewById(R.id.fragment_home), LogInActivity.this);
        phoneNumberTV.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    nextHandle();
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d("home","home");
                onBackPressed();
                break;
            case R.id.action_Next:
                nextHandle();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void nextHandle() {
        if (!phoneNumberTV.getText().toString().equals("") && phoneNumberTV.length() > 8) {
            if (checkInternetConnection(LogInActivity.this)) {
                mProgressBar.setVisibility(View.VISIBLE);
                Ulti.hideSoftKeyboard(LogInActivity.this);

                OkHttpClient.Builder client = new OkHttpClient.Builder();
                client.connectTimeout(15, TimeUnit.SECONDS);
                client.readTimeout(15, TimeUnit.SECONDS);
                client.writeTimeout(15, TimeUnit.SECONDS);

                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(Ulti.BaseUrl)
                                .addConverterFactory(GsonConverterFactory.create())
                                .client(client.build())
                                .build();

                         LoginService service = retrofit.create(LoginService.class);
                         Call call = service.getUser(phoneNumberTV.getText().toString());
                         call.enqueue(new Callback<LoginModel>() {
                             @Override
                             public void onResponse(@NonNull Call<LoginModel> call, @NonNull Response<LoginModel> response) {
                                 Log.d("response.code().", "" + response.code());
                                 if (response.body() != null && response.body().getStatus().equals("SuccessQuery")) {
                                     for (LoginDataClientsModel data : response.body().getLoginData().getClients()){
                                         Log.d("response.body().", "" + data.getUserName());

                                         String id = String.valueOf(data.getUserId());
                                         String name = data.getUserName();
                                         String alternateLookup = "";
                                         if(data.getUserAlterNateLookup() != null){
                                             alternateLookup = data.getUserAlterNateLookup();
                                         }else{
                                             alternateLookup = "No Number";
                                         }

                                         String dobOrigin = data.getUserDob();
                                         String dob;
                                         if (dobOrigin != null) {
                                             String[] separated = dobOrigin.split("T");
                                             dob = separated[0];
                                         } else {
                                             dob = "null";
                                         }

                                         String mobile = data.getUserMobile();
                                         String pointOrigin = String.valueOf(data.getUserPointValue());

                                         String email = data.getUserEmail();

                                         Log.d("mpbilename", "" + mobile + name);
                                         if (!Objects.equals(id, "") && !Objects.equals(name, "") && !Objects.equals(alternateLookup, "") &&
                                                 !Objects.equals(dob, "") && !Objects.equals(mobile, "") && !Objects.equals(pointOrigin, "")) {

                                             int cardimage = getResources().getIdentifier(Ulti.mSohoCardName, "drawable", LogInActivity.this.getPackageName());
                                             int logoimage = getResources().getIdentifier(Ulti.mSohoLogoName, "drawable", LogInActivity.this.getPackageName());
                                             databaseHelper.insert(id, name, dob, email, mobile, pointOrigin, Ulti.mSoho, Ulti.mSohoToken, Ulti.mSohoDkey, alternateLookup, cardimage, logoimage);
                                             databaseHelper.insertToken(email);
                                             Intent intent = new Intent(LogInActivity.this, LaunchScreenActivity.class);
                                             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                             startActivity(intent);
                                         } else {
                                             runOnUiThread(new Runnable() {
                                                 @Override
                                                 public void run() {
                                                     mProgressBar.setVisibility(View.INVISIBLE);
                                                     showMessage("Can not add this card because some fill is missing, Please check with post system.\nThank You!", LogInActivity.this);
                                                 }
                                             });
                                         }
                                     }
                                 }
                             }

                             @Override
                             public void onFailure(Call<LoginModel> call, Throwable t) {
                                 showMessage("No Internet Connection" + t, LogInActivity.this);
                             }
                         });
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setVisibility(View.INVISIBLE);
                    }
                });
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        showMessage("No Internet Connection", LogInActivity.this);
                    }
                });
            }
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgressBar.setVisibility(View.INVISIBLE);
                    showMessage("Invalid Phone Number or Card Number", LogInActivity.this);
                }
            });
        }
    }

    private void setmToolbar() {
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}
