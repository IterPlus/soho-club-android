package kh.com.soho.soho.SQLite;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Objects;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "card.sqlite";
    private static final String TABLE_NAME = "card_table";
    private static final String TOKEN_TABLE = "token_table";
    private static final String SYNC_TABLE = "sync_table";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE card_table (id TEXT, " +
                "name TEXT," +
                "dob TEXT," +
                "email TEXT," +
                "phone TEXT," +
                "point TEXT," +
                "outlet TEXT," +
                "token TEXT," +
                "dkey TEXT," +
                "serial TEXT,"+
                "cardimage INT,"+
                "logo INT"+ ")");

        sqLiteDatabase.execSQL("CREATE TABLE token_table (token TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE sync_table (sync TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + DATABASE_NAME + "'");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + TOKEN_TABLE + "'");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + SYNC_TABLE + "'");
        onCreate(sqLiteDatabase);
    }

    // sign in
    public void insertToken (String token) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("token", token);
        db.insert(TOKEN_TABLE, null, contentValues);
        Log.d("INSERT TOKEN = ","TOKEN" + token);
    }

    public void deleteToken () {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("delete TOKEN = ","deleteToken");
//        String.valueOf(db.delete(TOKEN_TABLE, "token = ? ", new String[]{token}));
        db.execSQL("delete from " + TOKEN_TABLE);
    }

    public Cursor queryToken(){
        SQLiteDatabase database = getReadableDatabase();
        Log.d("QUERY TOKEN = ","QUERY");
        return database.rawQuery("SELECT * FROM '"+ TOKEN_TABLE +"'", null);
    }

    //sync
    public void insertSync() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("sync", "sync");
        db.insert(SYNC_TABLE, null, contentValues);
        Log.d("INSERT SYNC = ","INSERT SYNC");
    }

    public void deleteSync () {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("DELETE SYNC = ","DELETE SYNC");
//        String.valueOf(db.delete(SYNC_TABLE, "sync = ? ", new String[]{token}));
        db.execSQL("delete from " + SYNC_TABLE);
    }

    public Cursor querySync(){
        SQLiteDatabase database = getReadableDatabase();
        Log.d("QUERY SYNC = ","QUERY SYNC");
        return database.rawQuery("SELECT * FROM '"+ SYNC_TABLE +"'", null);
    }


    //card
    public void insert (String id, String name, String dob, String email, String phone,String point ,String outlet, String token, String dkey, String serial, int image, int logo) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("name", name);
        contentValues.put("dob", dob);
        contentValues.put("email", email);
        contentValues.put("phone", phone);
        contentValues.put("point", point);
        contentValues.put("outlet", outlet);
        contentValues.put("token", token);
        contentValues.put("dkey", dkey);
        contentValues.put("serial", serial);
        contentValues.put("cardimage", image);
        contentValues.put("logo", logo);
        db.insert(TABLE_NAME, null, contentValues);

        Log.d("database","done insert");
    }

    public Cursor queryAll(){
        SQLiteDatabase database = getReadableDatabase();
        Log.d("database","done query");
        return database.rawQuery("SELECT * FROM '"+ TABLE_NAME +"'", null);
    }

    public boolean isCheckMember(String passedPhone,  String passedSerial){

        SQLiteDatabase database = getReadableDatabase();

        @SuppressLint("Recycle") Cursor cursor = database.rawQuery("SELECT * FROM '"+ TABLE_NAME +"'", null);

        while (cursor.moveToNext()) {
//            final String id = cursor.getString(0);
//            final String name = cursor.getString(1);
//            final String dob = cursor.getString(2);
//            String email = cursor.getString(3);
            final String phone = cursor.getString(4);
//            final String point = cursor.getString(5);
            final String outlet = cursor.getString(6);
//            final String token = cursor.getString(7);
//            final String dkey = cursor.getString(8);
            final String serial = cursor.getString(9);
//            final byte[] cardimage = cursor.getBlob(10);
//            final int logo = cursor.getInt(11);

            if(Objects.equals(phone, passedPhone)  && Objects.equals(passedSerial, serial)){
                return true;
            }
        }

        return false;
    }

    public void updatePoint (String point, String serial) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("point", point);
        db.update(TABLE_NAME, contentValues, "serial = ? ", new String[]{serial});
        Log.d("updatePoint = ","POINT UPDATED");
    }

    public void update (String id, String name, String phone) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("NAME", name);
        contentValues.put("PHONE", phone);
        db.update(TABLE_NAME, contentValues, "id = ? ", new String[]{id});
    }

    public void deleteAll () {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("DELETE CARD = ","DELETE CARD");
        db.execSQL("delete from " + TABLE_NAME);
    }

    public void delete (String id, String outlet) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, "id = ? AND outlet=? ", new String[]{id, outlet});
    }
}
