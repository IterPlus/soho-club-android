package kh.com.soho.soho;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class TabBarActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener, PromotionFragment.OnFragmentInteractionListener, HistoryFragment.OnFragmentInteractionListener, OutletFragment.OnFragmentInteractionListener{

    LockedViewPager viewPager;
    BottomNavigationView navigation;
    TabBarViewPagerAdapter adapter;
    static HomeModel list;
    static List<HistoryModel> historyLists;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_bar);

//        mToolbar = findViewById(R.id.act_tabbar_toolbar);
//        setmToolbar();

        navigation = findViewById(R.id.act_tabbar_tablayout);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        historyLists = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        list = bundle.getParcelable("dbquery");
        historyLists = bundle.getParcelableArrayList("db_history");
        viewPager = findViewById(R.id.act_tabbar_viewpager);
        adapter = new TabBarViewPagerAdapter(getSupportFragmentManager(), 4);
        viewPager.setSwipeable(false);
        viewPager.setAdapter(adapter);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_history:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_about:
                    viewPager.setCurrentItem(2);
                    return true;
                case R.id.navigation_location:
                    viewPager.setCurrentItem(3);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

}
