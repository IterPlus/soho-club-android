package kh.com.soho.soho;

import android.os.Parcel;
import android.os.Parcelable;

public class HistoryModel implements Parcelable {
    public String outlet;
    public String saleId;
    public String amount;
    public String payAmount;
    public String saleDate;
    public int outletID;

    public HistoryModel(String outlet, String saleId, String amount, String payAmount, String saleDate, int outletID) {
        this.outlet = outlet;
        this.saleId = saleId;
        this.amount = amount;
        this.payAmount = payAmount;
        this.saleDate = saleDate;
        this.outletID = outletID;
    }

    protected HistoryModel(Parcel in) {
        outlet = in.readString();
        saleId = in.readString();
        amount = in.readString();
        payAmount = in.readString();
        saleDate = in.readString();
        outletID = in.readInt();
    }

    public static final Creator<HistoryModel> CREATOR = new Creator<HistoryModel>() {
        @Override
        public HistoryModel createFromParcel(Parcel in) {
            return new HistoryModel(in);
        }

        @Override
        public HistoryModel[] newArray(int size) {
            return new HistoryModel[size];
        }
    };

    public String getOutlet() {
        return outlet;
    }

    public void setOutlet(String outlet) {
        this.outlet = outlet;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public int getOutletID() {
        return outletID;
    }

    public void setOutletID(int outletID) {
        this.outletID = outletID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(outlet);
        dest.writeString(saleId);
        dest.writeString(amount);
        dest.writeString(payAmount);
        dest.writeString(saleDate);
        dest.writeInt(outletID);
    }
}
