package kh.com.soho.soho;

public class AnnotationModel {

    String title;
    String locationName;
    int discipline;
    double longitude;
    double latitude;

    public AnnotationModel(String title, String locationName, int discipline, double longitude, double latitude) {
        this.title = title;
        this.locationName = locationName;
        this.discipline = discipline;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public int getDiscipline() {
        return discipline;
    }

    public void setDiscipline(int discipline) {
        this.discipline = discipline;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
