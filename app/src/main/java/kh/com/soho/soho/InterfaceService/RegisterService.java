package kh.com.soho.soho.InterfaceService;

import kh.com.soho.soho.Model.LoginModel;
import kh.com.soho.soho.Model.RegisterGetNameModel;
import kh.com.soho.soho.Model.RegisterUserModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RegisterService {

    @GET("token")
    Call<RegisterUserModel> getAccessToken();

    @FormUrlEncoded
    @POST("card/number")
    Call<RegisterGetNameModel> getKYCNumber(@Field("token") String token,
                                            @Field("card") String card,
                                            @Field("phone") String phone);
}
