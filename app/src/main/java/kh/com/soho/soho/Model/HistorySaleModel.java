package kh.com.soho.soho.Model;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class HistorySaleModel {
    private String outlet = "";
    private ArrayList<HistoryPurchaseDetailModel> sales;

    public String getOutlet(){
        return outlet;
    }

    public ArrayList<HistoryPurchaseDetailModel> getSales(){
        return sales;
    }
}
