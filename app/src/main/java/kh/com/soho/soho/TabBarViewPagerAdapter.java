package kh.com.soho.soho;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.io.Serializable;

public class TabBarViewPagerAdapter extends FragmentStatePagerAdapter{
    private int mNoOfTabs;

    TabBarViewPagerAdapter(FragmentManager fm, int mNoOfTabs) {
        super(fm);
        this.mNoOfTabs = mNoOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position)
        {
            case 0:
                HomeFragment cardFragment = new HomeFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable("key_id", TabBarActivity.list);
                cardFragment.setArguments(bundle);
                return cardFragment;
//                return new HomeFragment();
            case 1:
                HistoryFragment historyFragment = new HistoryFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("key_history", (Serializable) TabBarActivity.historyLists);
                historyFragment.setArguments(bundle1);
                return historyFragment;
//                return new HistoryFragment();
            case 2:
                return new PromotionFragment();
            case 3:
                return new OutletFragment();
            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return mNoOfTabs;
    }
}
