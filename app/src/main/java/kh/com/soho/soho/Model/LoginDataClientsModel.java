package kh.com.soho.soho.Model;

import com.google.gson.annotations.SerializedName;

public class LoginDataClientsModel {

    private int id = 0;
    private String name;
    private String alternateLookup = "";
    private String dob = "";
    private String mobile = "";
    private int pointValue = 0;
    private String email = "";

    public int getUserId(){
        return id;
    }

    public LoginDataClientsModel(String name){
        this.name = name;
    }

    public String getUserName(){
        return name;
    }

    public String getUserAlterNateLookup(){
        return alternateLookup;
    }

    public String getUserDob(){
        return dob;
    }

    public String getUserMobile(){
        return mobile;
    }

    public int getUserPointValue(){
        return pointValue;
    }

    public String getUserEmail(){
        return email;
    }
}
