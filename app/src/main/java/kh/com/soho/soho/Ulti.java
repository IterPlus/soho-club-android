package kh.com.soho.soho;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

public class Ulti {

    //Xilnex
    static String BaseUrl = "https://api.xilnex.com/logic/v2/";
    static String BaseUrlWing = "http://96.9.80.84:8181/api/request/";

    static String urlXilnexClientSearch = "https://api.xilnex.com/logic/v2/clients/search?code=";
    static String urlXilnexCreateClient = "https://api.xilnex.com/logic/v2/clients/client/";
    static String urlXilnexSalesInVoice = "https://api.xilnex.com/logic/v2/sales/";

    //Outlet card, logo and name
    static String mSohoCardName = "card_soho";
    static String mSohoLogoName = "logo_soho";
    static String mSoho = "Soho";

    //Pin
    static int mCrocsPin = R.drawable.crocs_pin;
    static int mSwatchPin = R.drawable.swatch_pin;
    static int mPenshoppePin = R.drawable.penshoppe_pin;
    static int mForMePin = R.drawable.forme_pin;
    static int mHavaianasPin = R.drawable.havaianas_pin;
    static int mMetallurgyPin = R.drawable.metallurgy_pin;
    static int mOxygenPin = R.drawable.oxygen_pin;
    static int mBonChonPin = R.drawable.bonchon_pin;
    static int mSanFranciscoPin = R.drawable.san_francisco_cafe_pin;
    static int mRokkuPin = R.drawable.rokku_pin;
    static int mInoriPin = R.drawable.inori_pin;
    static int mSprayWayPin = R.drawable.sprayway_pin;
    static int mBirkenStockPin = R.drawable.birkenstock_pin;
    static int mHauteRackPin = R.drawable.hauterack_pin;
    static String mAll = "ALL";

    //Xilnex Token
    static String mSohoToken = "24U3HurDqGbf1oi7nZZTsYkIQDEk2ZYP48x/TWAJ004=";
    //Xilnex Dkey
    static String mSohoDkey = "VDEHwLkcwg6pi1P4GUekHpbc4hB8Oh7C";
//    static String mSohoRequestToken = "http://192.168.1.12:8181/wingBridge/api/request/token";
//    static String mSohoDGetUserInfo = "http://192.168.1.12:8181/wingBridge/api/request/card/number";

    static String mSohoRequestToken = "http://96.9.80.84:8181/api/request/token";
    static String mSohoDGetUserInfo = "http://96.9.80.84:8181/api/request/card/number";


    //CROCS
    static String mCrocs = "CROCS";
    static String mCrocsAeonMallAddress = "Aeon Mall, 1st Floor, #132, St. Sothearos, Tonle Bassac, Chamkarmon, Phnom Penh";
    static String mCrocsAeonMallTitle = "CROCS Store AEON Mall Phnom Penh";
    static double mCrocsAeonMallLatitude =  11.5997746;
    static double mCrocsAeonMallLongitude = 104.8504244;

    static String mCrocsTKAvenueTitle = "CROCS Store TK Avenue";
    static String mCrocsTKAvenueAddress = "St. 315 (Corner St. 516), Toul Kork, Phnom Penh";
    static double mCrocsTKAvenueLatitude =  11.583628;
    static double mCrocsTKAvenueLongitude = 104.899152;

    static String mCrocsExchangeSquareTitle = "CROCS Store Exchange Square";
    static String mCrocsExchangeSquareAddress = "Exchange Square Mall, 1st Floor #28, St. 106 (Corner St. 51), Duan Penh, Phnom Penh";
    static double mCrocsExchangeSquareLatitude =  11.573696;
    static double mCrocsExchangeSquareLongitude = 104.921359;

    static String mCrocsSihanoukBlvdTitle = "CROCS Store Sihanouk Blvd";
    static String mCrocsSihanoukBlvdAddress = "No. 44 Sihanouk Blvd. Tonle Bassac Phnom Penh, Cambodia";
    static double mCrocsSihanoukBlvdLatitude =  11.554036;
    static double mCrocsSihanoukBlvdLongitude = 104.932678;

    static String mCrocsAEONMallSenSokTitle = "CROCS Store AEON Mall Sen Sok City";
    static String mCrocsAEONMallSenSokAddress = "AEON Mall Sen Sok City, Ground Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia";
    static double mCrocsAEONMallSenSokLatitude =  11.5996873;
    static double mCrocsAEONMallSenSokLongitude = 104.8832553;

    //FORME
    static String mForME = "FORME";
    static String mForMeAeonMallTitle = "FORME AEON Mall Phnom Penh";
    static String mForMeAeonMallAddress = "No. 132 Sothearos Blvd. Unit #0013 (Located on the 1st Floor), Phnom Penh";
    static double mForMeAeonMallLatitude =  11.5997746;
    static double mForMeAeonMallLongitude = 104.8504244;

    //SWATCH
    static String mSwatch = "SWATCH";
    static String mSwatchAeonMallTitle = "SWATCH AEON Mall Phnom Penh";
    static String mSwatchAeonMallAddress = "AEON Mall Ground Floor Fashion. #132, Street Samdach Sothearos, Sangkat Tonle Bassac, Khan Chamkarmon, Phnom Penh, Cambodia";
    static double mSwatchAeonMallLatitude =  11.5997746;
    static double mSwatchAeonMallLongitude = 104.8504244;

    static String mSwatchExchangeSquareTitle = "SWATCH Exchange Square";
    static String mSwatchExchangeSquareAddress = "Exchange Square Mall Ground Floor, St. 106 corner St. 61, Sangkat Wat Phnom, Khan Daun Penh, Phnom Penh, Cambodia";
    static double mSwatchExchangeSquareLatitude =  11.573840;
    static double mSwatchExchangeSquareLongitude = 104.921190;

    static String mSwatchAEONMallSenSokTitle = "SWATCH AEON Mall Sen Sok City";
    static String mSwatchAEONMallSenSokAddress = "AEON Mall Sen Sok City, Ground Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia.";
    static double mSwatchAEONMallSenSokLatitude =  11.5996873;
    static double mSwatchAEONMallSenSokLongitude = 104.8832553;

    //PENSHOPPE
    static String mPenshoppe = "PENSHOPPE";
    static String mPenshoppeAeonMallTitle = "PENSHOPPE AEON Mall Phnom Penh";
    static String mPenshoppeAeonMallAddress = "132 Street Samdach Sothearos, Sangkat Tonle Bassac, Khan Chamkarmon, Phnom Penh, Cambodia (Located on the Ground Floor)";
    static double mPenshoppeAeonMallLatitude =  11.5997746;
    static double mPenshoppeAeonMallLongitude = 104.8504244;

    static String mPenshoppeTKTitle = "PENSHOPPE TK Avenue";
    static String mPenshoppeTKAddress = "No. 80, St. 315, Corner of St. 516 Toul Kork, Phnom Penh, Cambodia, 12151";
    static double mPenshoppeTKLatitude =  11.583974;
    static double mPenshoppeTKLongitude = 104.899055;

    static String mPenshoppeLuckyMallTitle = "PENSHOPPE Lucky Mall";
    static String mPenshoppeLuckyMallAddress = "#E1-02, St. Sivutha ,Mondul II Village,Svay Dangkum | Siem Reap, Cambodia";
    static double mPenshoppeLuckyMallLatitude =  13.362713;
    static double mPenshoppeLuckyMallLongitude = 103.855515;

    static String mPenshoppeAeonMallSenSokTitle = "PENSHOPPE AEON Mall Sen Sok City";
    static String mPenshoppeAeonMallSenSokAddress = "AEON Mall 2 St. 1003, village Bayab, Commune, Phnom Penh";
    static double mPenshoppeAeonMallSenSokLatitude =  11.5996873;
    static double mPenshoppeAeonMallSenSokLongitude = 104.8832553;

    //BIRKENSTOCK
    static String mBirkenStock = "BIRKENSTOCK";
    static String mBirkenstockAEONMallSenSokTitle = "BIRKENSTOCK AEON Mall Sen Sok City";
    static String mBirkenstockAEONMallSenSokAddress = "AEON Mall Sen Sok City, Ground Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia";
    static double mBirkenstockAEONMallSenSokLatitude =  11.5996873;
    static double mBirkenstockAEONMallSenSokLongitude = 104.8832553;

    //HauteRackSquare
    static String mHauteRack = "HAUTE RACK";
    static String mHauteRackAeonSenSokTitle = "HAUTE RACK AEON Mall Sen Sok City";
    static String mHauteRackAeonSenSokAddress = "AEON Mall Sen Sok City, Ground Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia";
    static double mHauteRackAeonSenSokLatitude =  11.5996873;
    static double mHauteRackAeonSenSokLongitude = 104.8832553;

    //Havaianas
    static String mHavaianas = "HAVAIANAS";
    static String mHavaianasAEONMallTitle = "HAVAIANAS AEON Mall Phnom Penh";
    static String mHavaianasAEONMallAdddress = "No. 132 Sothearos Blvd. Unit #0013 (Located on the Ground Floor), Phnom Penh";
    static double mHavaianasAEONMallLatitude =  11.547784;
    static double mHavaianasAEONMallLongitude = 104.933320;

    static String mHavaianasPP63Title = "HAVAIANA BKK St 63";
    static String mHavaianasPP63Adddress = "No. 203 Street 63, Sangkat Beng Keng Kong I, Khan Chamkar Mon, Phnom Penh";
    static double mHavaianasPP63Latitude =  11.551682;
    static double mHavaianasPP63Longitude = 104.923650;

    static String mHavaianasBKKTitle = "HAVAIANAS BKK St 57";
    static String mHavaianasBKKAddress = "No. 6E0 Street 57, Sangkat Beng Keng Kong I, Khan Chamkar Mon, Phnom Penh";
    static double mHavaianasBKKLatitude =  11.554626;
    static double mHavaianasBKKLongitude = 104.924831;

    static String mHavaianasTKAvenueTitle = "HAVAIANAS TK Avenue";
    static String mHavaianasTKAvenueAddress = "No. 80, St. 315, Corner of St. 516 Khan Toul Kork, Phnom Penh";
    static double mHavaianasTKAvenueLatitude =  11.583358;
    static double mHavaianasTKAvenueLongitude = 104.899132;

    static String mHavaianasKingsRoadAngkorTitle = "HAVAIANAS Kings Road Angkor";
    static String mHavaianasKingsRoadAngkorAddress = "Corner of 7 Makara & Acha Sva Street, Sangkat Sala Komroek, Siem Reap";
    static double mHavaianasKingsRoadAngkorLatitude =  13.366616;
    static double mHavaianasKingsRoadAngkorLongitude = 103.860176;

    static String mHavaianasLuckyMallTitle = "HAVAIANAS Lucky Mall";
    static String mHavaianasLuckyMallAddress = "Sivutha Street, E1-02, Sangkat Svay Dangkum, Siem Reap";
    static double mHavaianasLuckyMallLatitude =  13.362131;
    static double mHavaianasLuckyMallLongitude = 103.855362;

    static String mHavaianasAngkorTicketCenterTitle = "HAVAIANAS Angkor Ticket Center";
    static String mHavaianasAngkorTicketCenterAddress = "St. 60, Trang Village, Sangkat Slor Kram, Siem Reap";
    static double mHavaianasAngkorTicketCenterLatitude =  13.377178;
    static double mHavaianasAngkorTicketCenterLongitude = 103.880508;

    static String mHavaianasExchangeSquareMallTitle = "HAVAIANAS Exchange Square Mall";
    static String mHavaianasExchangeSquareMallAddress = "#28 St. 106, Corner St. 51, Daun Penh, Phnom Penh";
    static double mHavaianasExchangeSquareMallLatitude =  11.573926;
    static double mHavaianasExchangeSquareMallLongitude = 104.921361;

    static String mHavaianasSoryaCenterPointTitle = "HAVAIANAS Sorya Center Point";
    static String mHavaianasSoryaCenterPointAddress = "#13-61, St. 63, Sangkat Phsar Thmei I, Khan Daun Penh, Phnom Penh";
    static double mHavaianasSoryaCenterPointLatitude =  11.567241;
    static double mHavaianasSoryaCenterPointLongitude = 104.920895;

    static String mHavaianasPhnomPenhAirportTitle = "HAVAIANAS Phnom Penh Airport";
    static String mHavaianasPhnomPenhAirportAddress = "12000 Angkor Phnom Penh St, Phnom Penh";
    static double mHavaianasPhnomPenhAirportLatitude =  11.552779;
    static double mHavaianasPhnomPenhAirportLongitude = 104.845058;

    static String mHavaianasAEONMallSenSokTitle = "HAVAIANAS AEON Mall Sen Sok City";
    static String mHavaianasAEONMallSenSokAddress = "AEON Mall Sen Sok City";
    static double mHavaianasAEONMallSenSokLatitude =  11.600647;
    static double mHavaianasAEONMallSenSokLongitude = 104.885638;

    //INORI
    static String mINoRiJewels = "INORI";
    static String mINoRiJewelsAEONMallSenSokTitle = "INORI AEON Mall Sen Sok City";
    static String mINoRiJewelsAEONMallSenSokAddress = "AEON Mall Sen Sok City, 1st Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia";
    static double mINoRiJewelsAEONMallSenSokLatitude =  11.5996873;
    static double mINoRiJewelsAEONMallSenSokLongitude = 104.8832553;

    //METALLURGY
    static String mMetallurgy = "METALLURGY";
    static String mMetallurgyAEONMallTitle = "METALLURGY AEON Mall Phnom Penh";
    static String mMetallurgyAEONMallAddress = "No. 132 Sothearos Blvd. Unit #0013 (Located on the 1st Floor), Phnom Penh";
    static double mMetallurgyAEONMallLatitude =  11.5997746;
    static double mMetallurgyAEONMallLongitude = 104.8504244;

    static String mMetallurgyTKAvenueTitle = "METALLURGY TK Avenue";
    static String mMetallurgyTKAvenueAddress = "No. 80, St. 315, Corner of St. 516, Khan Toul Kork, Phnom Penh";
    static double mMetallurgyTKAvenueLatitude =  11.5838252;
    static double mMetallurgyTKAvenueLongitude = 104.896952;

    //OXYGEN
    static String mOxygen = "OXYGEN";
    static String mOxygenAEONMallSenSokTitle = "OXYGEN AEON Mall Sen Sok City";
    static String mOxygenAEONMallSenSokAddress = "AEON Mall Sen Sok City, 1st Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia";
    static double mOxygenAEONMallSenSokLatitude =  11.5996873;
    static double mOxygenAEONMallSenSokLongitude = 104.8832553;

    //BONCHON
    static String mBonchon = "BONCHON";
    static String mBonchonPhnomPenhCenterTitle = "BONCHON Phnom Penh Center";
    static String mBonchonPhnomPenhCenterAddress = "Phnom Penh Center, Sothearos Blvd. (Near Build Bright University) Phnom Penh Cambodia";
    static double mBonchonPhnomPenhCenterLatitude =  11.555328;
    static double mBonchonPhnomPenhCenterLongitude = 104.930673;

    static String mBonchonAeonMallTitle = "BONCHON Aeon Mall Phnom Penh";
    static String mBonchonAeonMallAddress = "No. 132 Sothearos Blvd. Unit #0013 (Located on the Ground Floor), Phnom Penh";
    static double mBonchonAeonMallLatitude =  11.5997746;
    static double mBonchonAeonMallLongitude = 104.8504244;

    static String mBonchonBKKTitle = "BONCHON BKK";
    static String mBonchonBKKAddress = "No. 80, Sangkat Beung Kak 1, Khan Toul Kork, Phnom Penh, Cambodia";
    static double mBonchonBKKLatitude = 11.5833789;
    static double mBonchonBKKLongitude = 104.8968343;

    static String mBonchonRiverSideTitle = "BONCHON RiverSide";
    static String mBonchonRiverSideAddress = "Riverside, No. 386 Theamak Lethet Ouk (184) Phnom Penh, Cambodia";
    static double mBonchonRiverSideLatitude =  11.5659663;
    static double mBonchonRiverSideLongitude = 104.9297762;

    static String mBonchonTKTitle = "BONCHON TK";
    static String mBonchonTKAddress = "No. 80, Sangkat Beung Kak 1, Khan Toul Kork, Phnom Penh, Cambodia";
    static double mBonchonTKLatitude =  11.5833789;
    static double mBonchonTKLongitude = 104.8968343;

    static String mBonchonAeonMaxValueExpressTitle = "BONCHON AEON MaxValu Express";
    static String mBonchonAeonMaxValueExpressAddress = "No. 79, Street 315, Phum 6, Sangkat Boeung Kok 2, Khan Toul Kork, Phnom Penh, Cambodia";
    static double mBonchonAeonMaxValueExpressLatitude =  11.5749106;
    static double mBonchonAeonMaxValueExpressLongitude = 104.8822669;

    static String mBonchonExchangeSquareTitle = "Bonchon Exchange Square";
    static String mBonchonExchangeSquareAddress = "#S3-01, Second Floor, St. 51-61 and St. 102-106, Sangkat Wat Phnom, Khan Daun Penh, Phnom Penh";
    static double mBonchonExchangeSquareLatitude =  11.5740449;
    static double mBonchonExchangeSquareLongitude = 104.9187677;

    static String mBonchonIFLTitle = "BONCHON IFL";
    static String mBonchonIFLAddress = "No. 120C1, St. 110 (Russian Blvd.) Sangkat Tek Laak 1, Khan Toul Kork, Phnom Penh, Cambodia";
    static double mBonchonIFLLatitude =  11.5679605;
    static double mBonchonIFLLongitude = 104.8915755;

    static String mBonchonAttwoodTitle = "BONCHON Attwood";
    static String mBonchonAttwoodAddress = "Unit No. 15 #31E0, Russian Blvd, Sangkat Teok Thla, Khan Sen Sok, Phnom Penh, Cambodia";
    static double mBonchonAttwoodLatitude =  11.5620619;
    static double mBonchonAttwoodLongitude = 104.8698257;

    static String mBonchonSovannaTitle = "BONCHON Sovanna";
    static String mBonchonSovannaAddress = "Sovanna Shopping Center. Ground floor, Unit S1-02/06/08. #307-309, Street 271, Sangkat Tumnob Teuk, Khan Chamkar Morn, Phnom Penh, Cambodia";
    static double mBonchonSovannaLatitude =  11.5412539;
    static double mBonchonSovannakLongitude = 104.8803398;

    static String mBonchonStoengMeancheyTitle = "BONCHON Stoeng Meanchey";
    static String mBonchonStoengMeancheyAddress = "Lot 0013-2 #1024AE0, Street 217 Sangkat Stung Meanchey, Khan Meanchey Phnom Penh, Cambodia";
    static double mBonchonStoengMeancheyLatitude =  11.5413369;
    static double mBonchonStoengMeancheyLongitude = 104.8803398;

    static String mBonchonSoryaMallTitle = "BONCHON Sorya Mall";
    static String mBonchonSoryaMallAddress = "Ground floor. Unit FG-F07-08, #13-61 Street 63, Sangkat Phsar Thmey I Khan Daun Penh, Phnom Penh, Cambodia";
    static double mBonchonSoryaMallLatitude =  11.54142;
    static double mBonchonSoryaMallLongitude = 104.8803398;

    static String mBonchonAeonMallSenSokCityTitle = "BONCHON AEON Mall Sen Sok City";
    static String mBonchonAeonMallSenSokCityAddress = "AEON Mall Sen Sok City, 2nd Floor, St. 1003 Village Bayab Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia";
    static double mBonchonAeonMallSenSokLatitude =  11.5996873;
    static double mBonchonAeonMallSenSokLongitude = 104.8832553;

    //SPRAYWAY
    static String mSprayWay = "SPRAYWAY";
    static String mSprayWayAEONMallSenSokTitle = "SPRAYWAY AEON Mall Sen Sok City";
    static String mSprayWayAEONMallSenSokAddress = "AEON Mall Sen Sok City, 1st Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia";
    static double mSprayWayAEONMallSenSokLatitude =  11.5996873;
    static double mSprayWayAEONMallSenSokLongitude = 104.8832553;

    //SAN FRANCISCO CAFFE
    static String mSanFrancisco = "SAN FRANCISCO CAFFE";
    static String mSanFranciscoExchangeSquareTitle = "SAN FRANCISCO CAFFE Exchange Square";
    static String mSanFranciscoExchangeSquareAddress = "No. 28, Street 106 (Corner Street 51), Sangkat Wat Phnom, Khan Duan Penh, Phnom Penh, Cambodia";
    static double mSanFranciscoExchangeSquareLatitude =  11.574024;
    static double mSanFranciscoExchangeSquareLongitude = 104.921338;

    static String mSanFranciscoAEONMallSenSokTitle = "SAN FRANCISCO CAFFE AEON Mall Sen Sok City";
    static String mSanFranciscoAEONMallSenSokAddress = "AEON Mall Sen Sok City, Ground Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia.";
    static double mSanFranciscoAEONMallSenSokLatitude =  11.5996873;
    static double mSanFranciscoAEONMallSenSokLongitude = 104.8832553;

    //ROKKU
    static String mRokku = "Rokku Sushi Lounge and Bistro";
    static String mRokkuTitle = "Rokku Sushi Lounge and Bistro";
    static String mRokkuAddress = "No. 507 Sisowath Quay Corner of Suramarit Blvd. Phnom Penh, Cambodia";
    static double mRokkuLatitude =  11.5570174;
    static double mRokkuLongitude = 104.9313678;

    //SOHO promotion website
    static String mPromoBirkenstock = "http://www.demo.sohoclub.com.kh/brands/birkenstock";
    static String mPromoBonchon = "http://www.demo.sohoclub.com.kh/brands/bonchon";
    static String mPromoCrocs = "http://www.demo.sohoclub.com.kh/brands/crocs";
    static String mPromoForme = "http://www.demo.sohoclub.com.kh/brands/forme";
    static String mPromoHauteRack = "http://www.demo.sohoclub.com.kh/brands/haute-rack";
    static String mPromoHavaianas = "http://www.demo.sohoclub.com.kh/brands/havaianas";
    static String mPromoInori = "http://www.demo.sohoclub.com.kh/brands/inori-jewels";
    static String mPromoMetallurgy = "http://www.demo.sohoclub.com.kh/brands/metallurgy";
    static String mPromoOxygen = "http://www.demo.sohoclub.com.kh/brands/oxygen";
    static String mPromoPenshoppe = "http://www.demo.sohoclub.com.kh/brands/penshoppe";
    static String mPromoRokku = "http://www.demo.sohoclub.com.kh/brands/rokku";
    static String mPromoSanFrancisco = "http://www.demo.sohoclub.com.kh/brands/san-francisco-caffe";
    static String mPromoSprayWay = "http://www.demo.sohoclub.com.kh/brands/sprayway";
    static String mPromoSwatch = "http://www.demo.sohoclub.com.kh/brands/swatch";

    //Soho website
    static String mSohoWebsite = "http://www.soho.com.kh/about-us/";

    static String urlXilnexSaleHistory(String id){
        return "https://api.xilnex.com/logic/v2/clients/" + id + "/sales";
    }

    static boolean checkInternetConnection(Context context){
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm != null ? cm.getActiveNetworkInfo() : null;
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    static byte[] byteArrayConvertor(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        return outputStream.toByteArray();
    }

    static void showMessage(final String message, Context context) {
        new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                    }).show();
    }

     static Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, 1000, 1000, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();

        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 1000, 0, 0, w, h);
        return bitmap;
    }

    public static void setupUI(View view, final Activity activity) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView, activity);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        assert inputMethodManager != null;
        inputMethodManager.hideSoftInputFromWindow(Objects.requireNonNull(activity.getCurrentFocus()).getWindowToken(), 0);
    }
}
