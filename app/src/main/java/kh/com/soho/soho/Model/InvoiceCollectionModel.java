package kh.com.soho.soho.Model;

public class InvoiceCollectionModel {
    private String paymentDate = "";
    private String receivedBy = "";

    public String getPaymentDate(){
        return paymentDate;
    }

    public String getReceivedBy(){
        return receivedBy;
    }
}