package kh.com.soho.soho;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.r0adkll.slidr.Slidr;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CreateUserActivity extends AppCompatActivity {

    Toolbar mToolbar;
    String lastName, firstName, phone;
    EditText nameEditText, phoneEditText, emailEditText;
    Spinner genderSpinner;
    Button verifyBtn;
    String selectedGender = "Male";
    String[] gender = {"Male", "Female"};
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
        mToolbar = findViewById(R.id.act_create_user_toolbar);
        nameEditText = findViewById(R.id.act_create_name_edittext);
        phoneEditText = findViewById(R.id.act_create_phone_edittext);
        genderSpinner = findViewById(R.id.act_create_gender_spinner);
        emailEditText = findViewById(R.id.act_create_emaail_edittext);
        verifyBtn = findViewById(R.id.act_create_verify_btn);
        progressBar = findViewById(R.id.act_create_progress_bar);
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        firstName = bundle.getString("first_name");
        lastName = bundle.getString("last_name");
        phone = bundle.getString("phone");
        setmToolbar();
        Ulti.setupUI(findViewById(R.id.act_create_user_parent), CreateUserActivity.this);
        String name = firstName + lastName;
        nameEditText.setText(name);
        phoneEditText.setText(phone);
        Slidr.attach(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.gender_snipper_item, gender);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(adapter);

        verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        createUser();
                    }
                }, 500);
            }
        });

        emailEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    createUser();
                }
                return false;
            }
        });
    }

    void createUser(){
        if(!nameEditText.getText().toString().equals("") && !phoneEditText.getText().toString().equals("") && !emailEditText.getText().toString().equals("")) {
            progressBar.setVisibility(View.VISIBLE);
            try {

                new JSONObject(new CreateUserAsynTask(CreateUserActivity.this).execute(firstName, lastName, emailEditText.getText().toString(), selectedGender, phoneEditText.getText().toString()).get());

            } catch (JSONException | InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }else{
            new android.app.AlertDialog.Builder(this)
                    .setTitle("Alert")
                    .setMessage("Please fill all information below.")
                    .setCancelable(false)
                    .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setmToolbar() {
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void checkSuccess(boolean success){
        if(success) {
            progressBar.setVisibility(View.INVISIBLE);

            new android.app.AlertDialog.Builder(CreateUserActivity.this)
                    .setTitle("Congratulation")
                    .setMessage("Now you has become a soho club member.")
                    .setCancelable(true)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent = new Intent(CreateUserActivity.this, WelcomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            }).start();
                        }
                    }).show();

        }else {
            progressBar.setVisibility(View.INVISIBLE);
            Log.d("Failed","Failed");
        }
    }
}

class CreateUserAsynTask extends AsyncTask<String, String, String> {

    private OkHttpClient client = new OkHttpClient();
    private Response response;
    private String jsonData = "";
    @SuppressLint("StaticFieldLeak")
    private CreateUserActivity createUserActivity;

    CreateUserAsynTask(CreateUserActivity activity)
    {
        this.createUserActivity = activity;
    }
    @Override
    protected String doInBackground(String... params) {
        //        params[0] = first
        //        params[1] = last
        //        params[2] = email
        //        params[3] = geder
        //        params[4] = phone
        String name = params[0] + params[1];
        try {
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{  \n   \"client\":{  \n      \"name\":\""+name+"\",\n      \"email\":\""+params[2]+"\",\n      \"gender\":\""+params[3]+"\",\n      \"firstName\":\""+params[0]+"\",\n      \"lastName\":\""+params[1]+"\",\n      \"mobile\":\""+params[4]+"\",\n      \"active\":true,\n      \"allowAllOutlets\":true,\n      \"createdOutlet\":\"Main Branch\",\n      \"pointFactor\":1\n   }\n}");
            Request request = new Request.Builder()
                    .url(Ulti.urlXilnexCreateClient)
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("dkey", Ulti.mSohoDkey)
                    .addHeader("token", Ulti.mSohoToken)
                    .addHeader("Cache-Control", "no-cache")
                    .addHeader("Postman-Token", "b8d0f33b-80ff-43e0-aa3a-e052848f10fb")
                    .build();

            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(response != null) {
                jsonData = response.body().string();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonData;
    }

    @Override
    protected void onPostExecute(String result) {
        if(!result.equals("")){
            createUserActivity.checkSuccess(true);
        }
    }

    @Override
    protected void onPreExecute() {

    }
}