package kh.com.soho.soho;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.concurrent.ExecutionException;

import kh.com.soho.soho.SQLite.DatabaseHelper;

public class WelcomeActivity extends AppCompatActivity {

    Button logInBtn, registerBtn;
    DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        logInBtn = findViewById(R.id.act_welcome_login_button);
        registerBtn = findViewById(R.id.act_welcome_register_button);

        databaseHelper = new DatabaseHelper(this);

        //Check login Token
//        Cursor cursor = databaseHelper.queryToken();
//        while (cursor.moveToNext()) {
//            if (databaseHelper.queryToken() != null) {
//                Intent intent = new Intent(WelcomeActivity.this, LaunchScreenActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//            }
//        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        logInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeActivity.this, LogInActivity.class);
                startActivity(intent);
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }
}
