package kh.com.soho.soho.Model;

public class HistoryPurchaseDetailModel {

    private int salesId = 0;
    private double amount = 0.0;
    private double paidAmount = 0.0;
    private String salesDate = "";

    public int getSalesId(){
        return salesId;
    }

    public double getSaleAmount(){
        return amount;
    }

    public double getSalePaidAmount(){
        return paidAmount;
    }

    public String getSaleDate(){
        return salesDate;
    }
}
