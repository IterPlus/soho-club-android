package kh.com.soho.soho;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import kh.com.soho.soho.Custom.MyItemDecoration;
import kh.com.soho.soho.InterfaceService.LoadingHistoryService;
import kh.com.soho.soho.Model.HistoryPurchaseDetailModel;
import kh.com.soho.soho.Model.HistorySaleModel;
import kh.com.soho.soho.Model.LoadingHistoryModel;
import kh.com.soho.soho.SQLite.DatabaseHelper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static kh.com.soho.soho.Ulti.checkInternetConnection;
import static kh.com.soho.soho.Ulti.showMessage;

public class HistoryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    List<HistoryModel> historyLists;
    HistoryAdapter adapter;
    private RecyclerView mRecyclerView;
    DatabaseHelper databaseHelper;
    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefreshLayout;
    String id = "";
    int logo;
    @SuppressWarnings("FieldCanBeLocal")
    private LinearLayoutManager mLinearLayoutManager;
    private OnFragmentInteractionListener mListener;

    public static HistoryFragment newInstance(String param1, String param2) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = new DatabaseHelper(getContext());
        loadData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_history, container, false);

        progressBar = v.findViewById(R.id.fragment_history_progress_bar);
        mRecyclerView = v.findViewById(R.id.fragment_history_recycler);
        swipeRefreshLayout = v.findViewById(R.id.fragment_history_container);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            historyLists = (List<HistoryModel>) getArguments().getSerializable("key_history");
        }
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.addItemDecoration(new MyItemDecoration());
        adapter = new HistoryAdapter(historyLists, getContext());
        mRecyclerView.setAdapter(adapter);
        swipeRefreshLayout.setOnRefreshListener(this);
        return v;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        historyLists.clear();
        loadHistory();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void loadData() {
        Cursor cursor = databaseHelper.queryAll();
        while (cursor.moveToNext()) {
            id = cursor.getString(0);
            logo = cursor.getInt(11);
        }
    }

    void loadHistory(){

        if (checkInternetConnection(Objects.requireNonNull(getContext()))) {
            OkHttpClient.Builder client = new OkHttpClient.Builder();
            client.connectTimeout(15, TimeUnit.SECONDS);
            client.readTimeout(15, TimeUnit.SECONDS);
            client.writeTimeout(15, TimeUnit.SECONDS);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Ulti.BaseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client.build())
                    .build();

            LoadingHistoryService service = retrofit.create(LoadingHistoryService.class);
            Call call = service.getHistorySale(Ulti.urlXilnexSaleHistory(id));
            call.enqueue(new Callback<LoadingHistoryModel>() {
                @Override
                public void onResponse(@NonNull Call<LoadingHistoryModel> call, @NonNull retrofit2.Response<LoadingHistoryModel> res) {
                    if (res.body() != null) {
                        // get Purchase History
                        for (HistorySaleModel purchase : res.body().getHistoryData().getPurchaseHistories()){
                            // get sales Detail
                            for (HistoryPurchaseDetailModel sale : purchase.getSales()){
                                String saleId = String.valueOf(sale.getSalesId());
                                String amount = String.valueOf(sale.getSaleAmount());
                                String paidAmount = String.valueOf(sale.getSalePaidAmount());
                                String salesDate = sale.getSaleDate();
                                String[] splitSaleDate = salesDate.split("T00");

                                historyLists.add(new HistoryModel(purchase.getOutlet(), saleId, amount, paidAmount, splitSaleDate[0].trim(), logo));
                                swipeRefreshLayout.setRefreshing(false);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoadingHistoryModel> call, Throwable t) {
                    showMessage("No Internet Connection" + t, getContext());
                }
            });

        }else{
            showMessage("No Internet Connection", getContext());
            hideLoading();
            swipeRefreshLayout.setRefreshing(false);
        }

        hideLoading();
    }

    void hideLoading() {
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    void showLoading() {
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }
}