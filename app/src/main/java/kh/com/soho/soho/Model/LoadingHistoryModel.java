package kh.com.soho.soho.Model;

public class LoadingHistoryModel {
        private HistoryPurchaseModel data;

    public LoadingHistoryModel(HistoryPurchaseModel data) {
        this.data = data;
    }

    public HistoryPurchaseModel getHistoryData(){
            return data;
        }
}
