package kh.com.soho.soho;

import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class PopUpWebsiteActivity extends AppCompatActivity {
    String passedUrl;
    Toolbar mToolbar;
    WebView webView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_website);

        webView = findViewById(R.id.act_pop_up_website_webview);
        progressBar = findViewById(R.id.act_pop_up_website_progress_bar);
        mToolbar = findViewById(R.id.act_pop_up_website_toolbar);
        setmToolbar();

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        passedUrl = bundle.getString("promotion");

        assert passedUrl != null;
        switch (passedUrl){
            case "pro_birkenstock":
                passedUrl = Ulti.mPromoBirkenstock;
                break;
            case "pro_bonchon":
                passedUrl = Ulti.mPromoBonchon;
                break;
            case "pro_crocs":
                passedUrl = Ulti.mPromoCrocs;
                break;
            case "pro_forme":
                passedUrl = Ulti.mPromoForme;
                break;
            case "pro_hauterack":
                passedUrl = Ulti.mPromoHauteRack;
                break;
            case "pro_havaianas":
                passedUrl = Ulti.mPromoHavaianas;
                break;
            case "pro_inori":
                passedUrl = Ulti.mPromoInori;
                break;
            case "pro_metallurgy":
                passedUrl = Ulti.mPromoMetallurgy;
                break;
            case "pro_oxgn":
                passedUrl = Ulti.mPromoOxygen;
                break;
            case "pro_penshoppe":
                passedUrl = Ulti.mPromoPenshoppe;
                break;
            case "pro_rokku":
                passedUrl = Ulti.mPromoRokku;
                break;
            case "pro_sanfrancisco":
                passedUrl = Ulti.mPromoSanFrancisco;
                break;
            case "pro_sprayway":
                passedUrl = Ulti.mPromoSprayWay;
                break;
            case "pro_swatch":
                passedUrl = Ulti.mPromoSwatch;
                break;
                default:
                    break;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                webView.setWebViewClient(new WebViewClient());
                webView.loadUrl(passedUrl);
                webView.setWebViewClient(new WebViewClient() {

                    public void onPageFinished(WebView view, String url) {
                        // do your stuff here
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        });
                    }
                });
            }
        }, 500);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_card_detail, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setmToolbar() {
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}
